<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class Auth {
	var $CI = null;

	function Auth(){
		$this->CI =& get_instance();
		$this->CI->load->database();
	}

	function do_login($login = NULL){

     	// A few safety checks
    	// Our array has to be set
    	if(!isset($login))
	        return FALSE;
		
    	//Our array has to have 2 values
     	//No more, no less!
    	if(count($login) != 2)
		return FALSE;
	
     	$username = $login['username'];
     	$password = md5($login['password']);

        $query=$this->CI->db->get_where('user',
			array('username'=>$username, 'password'=>$password));

     	foreach ($query->result() as $row){
    		$user_id        	= $row->user_id;
			$username       	= $row->username;
			$fullname       	= $row->fullname;
			$level          	= $row->id_usergroup;
    	}

     	if ($query->num_rows() == 1){
       	 	$newdata = array(
       	 	    'user_id'		=> $user_id,
              	'username'  	=> $username,
                'fullname'  	=> $fullname,
                'logged_in' 	=> TRUE,
		    	'level'			=> $level
           		);
			// Our user exists, set session.
			$this->CI->session->set_userdata($newdata);	  
			
			return TRUE;
		} else {
			// No existing user.
			return FALSE;
		}
	}

	function logout(){
		$this->CI->session->sess_destroy();	
		return TRUE;
	}

	function restrict($logged_out = FALSE){

		// If the user is logged in and he's trying to access a page
		// he's not allowed to see when logged in,
		// redirect him to the index!
		if ($logged_out && is_logged_in()){
	      	echo $this->CI->fungsi->warning('Maaf, sepertinya Anda sudah login...',site_url());
	      	die();
		}
		
		// If the user isn' logged in and he's trying to access a page
		// he's not allowed to see when logged out,
		// redirect him to the login page!
		if ( ! $logged_out && !is_logged_in()){
		    echo $this->CI->fungsi->warning('Anda diharuskan untuk Login bila ingin mengakses halaman ini.',site_url());
		    die();
		}
	}

	function checkPermission($menu, $permission, $id_usergroup){

		$sql = $this->CI->db->query("SELECT * from auth where menu = '".$menu."' and permission = '".$permission."' and id_usergroup = '".$id_usergroup."'");

        if($sql->num_rows()>0){
            return TRUE;
        }else{
            return FALSE;
        }

	}
}