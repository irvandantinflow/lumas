<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function is_logged_in(){
	$CI =& get_instance();
	$ss = $CI->session->userdata('logged_in');
	if($ss != '')
	{
	    return TRUE;
	}
	return FALSE;
}

function is_access(){
	$CI =& get_instance();
	$level = $CI->session->userdata('level');
	if($level == 1)
	{
	    return TRUE;
	}
	return FALSE;
}

// function unformat number 
function number_unformat($number, $force_number = true, $dec_point = ',', $thousands_sep = '.') {
	if ($force_number) {
		$number = preg_replace('/^[^\d]+-/', '', $number);
	} else if (preg_match('/^[^\d]+-/', $number)) {
		return false;
	}
	$type = 'float';// (strpos($number, $dec_point) === false) ? 'int' : 'float';
	// $number = str_replace(array($dec_point, $thousands_sep), array('.', ''), $number);
	$number = str_replace($thousands_sep, '', $number);
	$number = str_replace($dec_point, '.', $number);
	settype($number, $type);
	return $number;
}

// Get Select Options Data
function getOptions($table='', $key='', $value='', $where='', $query='', $order_by='') 
{
	$_this     = & get_Instance();
	$where     = ($where) ? " WHERE ".$where : "";
    $order_by  = ($order_by) ? " ORDER BY ".$order_by : ""; 
    if ($query != '') {
        $rows = $_this->db->query($query)->result();
    } else {
        $rows = $_this->db->query("SELECT ".$key.", ".$value." FROM ".$table." ".$where." ".$order_by)->result();   
    }
	$retData[''] = "";
	foreach($rows as $row)
	{  
		$retData[$row->$key] = $row->$value;
	} 
	return $retData;
}