<div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('Home')?>" class="site_title"> <span>Lumas Jaya</span></a>
            </div>

            <div class="clearfix"></div>
            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <ul class="nav side-menu">
                    <?php if($this->session->userdata('level') == '1') { ?>
                      <li class="<?php echo ($active == 'Purchasing') ? 'active' : ''; ?>"><a><i class="fa fa-table"></i> Purchasing <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" style="<?php echo ($active == 'Purchasing') ? 'display:block' : ''; ?>">
                          <li class="<?php echo activate_menu('Purchase'); ?>"><a href="<?php echo base_url('Purchase'); ?>">Purchase Order</a></li>
                          <li class="<?php echo activate_menu('Retur'); ?>"><a href="<?php echo base_url('Retur'); ?>">Retur</a></li>
                          <li class="<?php echo activate_menu('Supplier'); ?>"><a href="<?php echo base_url('Supplier'); ?>">Supplier</a></li>
                        </ul>
                      </li>
                      <li class="<?php echo ($active == 'Sales') ? 'active' : ''; ?>"><a><i class="fa fa-money"></i> Sales <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" style="<?php echo ($active == 'Sales') ? 'display:block' : ''; ?>">
                          <li class="<?php echo activate_menu('Salesorder'); ?>"><a href="<?php echo base_url('Salesorder'); ?>">Sales Order</a></li>
                          <li class="<?php echo activate_menu('Customer'); ?>"><a href="<?php echo base_url('Customer'); ?>">Customer</a></li>
                        </ul>
                      </li>
                      <li class="<?php echo ($active == 'Inventory') ? 'active' : ''; ?>"><a><i class="fa fa-cubes"></i> Inventory <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" style="<?php echo ($active == 'Inventory') ? 'display:block' : ''; ?>">
                          <li class="<?php echo activate_menu('Deliveryorder'); ?>"><a href="<?php echo base_url('Deliveryorder'); ?>">Delivery Order</a></li>
                          <li class="<?php echo activate_menu('Produk'); ?>"><a href="<?php echo base_url('Produk'); ?>">Produk</a></li>
                          <li class="<?php echo activate_menu('Stock'); ?>"><a href="<?php echo base_url('Stock'); ?>" >Stock</a></li>
                          <li class="<?php echo activate_menu('Stock_opname'); ?>"><a href="<?php echo base_url('Stock_opname'); ?>">Stock Opname</a></li>
                          <li class="<?php echo activate_menu('Stock_opname/kartu_stock'); ?>"><a href="<?php echo base_url('Stock_opname/kartu_stock'); ?>">Kartu Stock</a></li>
                        </ul>
                      </li>
                      <li class="<?php echo ($active == 'Report') ? 'active' : ''; ?>"><a><i class="fa fa-bar-chart-o"></i> Laporan <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" style="<?php echo ($active == 'Report') ? 'display:block' : ''; ?>">
                          <li class="<?php echo activate_menu('Purchase/report'); ?>"><a href="<?php echo base_url('Purchase/report'); ?>">Purchasing</a></li>
                          <li class="<?php echo activate_menu('Stock/inventory'); ?>"><a href="<?php echo base_url('Stock/inventory'); ?>">Inventory</a></li>
                          <li class="<?php echo activate_menu('Salesorder/report'); ?>"><a href="<?php echo base_url('Salesorder/report'); ?>">Sales</a></li>
                        </ul>
                      </li>
                      <li class="<?php echo ($active == 'Master') ? 'active' : ''; ?>"><a><i class="fa fa-cogs"></i> Master <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" style="<?php echo ($active == 'Master') ? 'display:block' : ''; ?>">
                          <li class="<?php echo activate_menu('User'); ?>"><a href="<?php echo base_url('User'); ?>">User</a></li>
                          <li class="<?php echo activate_menu('Usergroup'); ?>"><a href="<?php echo base_url('Usergroup'); ?>">Usergroup</a></li>
                          <li class="<?php echo activate_menu('Kurir'); ?>"><a href="<?php echo base_url('Kurir'); ?>">Kurir</a></li>
                          <li class="<?php echo activate_menu('Harga'); ?>"><a href="<?php echo base_url('Harga'); ?>">Harga</a></li>
                        </ul>
                      </li>
                    <?php }?>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <?php echo $this->session->userdata('fullname') ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="<?php echo base_url('Inadminpage/logout'); ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->