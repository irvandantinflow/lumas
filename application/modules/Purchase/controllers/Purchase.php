<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchase extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Purchase_model');
		if(!is_logged_in()){redirect('Inadminpage');}	
		// if(!is_access()){
		// 	$this->session->set_flashdata('error', 'Anda tidak berhak mengakses halaman ini');
		// 	redirect('Home');
		// }
	}

	public function index(){	
		
	   	$data = array(
	   		'active'	=> 'Purchasing',
	   		'content'	=> 'Purchase/list_purchase',
	   		'title'		=> 'Purchase',
	   		'list'		=> $this->Purchase_model->getList()
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	public function report(){	
		$list=''; 
		if (isset($_POST['submit'])){
			$filterParam = $this->fungsi->accept_data(array_keys($_POST));  
			if ($_POST['submit'] == 'findData'){ 
				$list = $this->Purchase_model->getListReport($filterParam);
			}elseif($_POST['submit'] == 'exportToExcel'){	 
				$this->excel_this($filterParam);
					return true;
			}  
		} else {
			$filterParam = '';
		}
		
	   	$data = array(
	   		'active'	=> 'Report',
	   		'content'	=> 'Purchase/list_purchase_report',
	   		'title'		=> 'Report Purchase',
	   		'list'		=> $list,
	   		'supplier'	=> $this->Purchase_model->getSupplier(),
	   		'param'     => $filterParam
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	function excel_this($data = ''){
		 
		if($data == ''){
			$this->session->set_flashdata('error', 'Tidak ada parameter');
			redirect('Purchase/list_purchase_report');
		}
		$this->load->library('to_excel');
		
		if($data['id_supplier'] != '')	$namasup = $this->db->query("SELECT * FROM supplier WHERE id_supplier = '" . $data['id_supplier'] . "'")->row()->nama_supplier;
		else							$namasup = '- Semua Supplier -';
		
		
		$title	 = '<div style="font-size: 24px"><strong>PT LUMAS JAYA INDUSTRI</strong></div>';
		$title	.= '<div style="font-size: 18px"><strong>Laporan Purchase Order (PO)</strong></div><br />';
		$title	.= '<strong>Tanggal Cetak Report: ' . date('d-m-Y') . '</strong><br />';
		$title	.= '<strong>Periode Report: ' . $data['start_date'] . ' s/d ' . $data['end_date'] . '</strong><br />';
		$title	.= '<strong>Nama Supplier : ' . $namasup . '</strong><br />'; 
		$title	.= '<br />';
		
		$dataRpt = $this->Purchase_model->getListReport($data, TRUE)->result_array();
		to_excel($dataRpt,
			'PO_Report-' . date('dmY'),
			$title
		);
	}	

	public function add(){	
		
	   	$data = array(
	   		'active'		=> 'Purchasing',
	   		'content'		=> 'Purchase/form_purchase',
		   	'title'			=> 'New Purchase',
		   	'supplier'		=> 	$this->Purchase_model->getSupplier(),
		   	'page_action' 	=> base_url('Purchase/add_process'),
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	function add_process(){
		
		$data 	= $this->fungsi->accept_data(array_keys($_POST));

		//detail data
		$order = $data['order'];
		$i = 0;
		foreach($order as $row)
		{
			$data_detail[] = array(
				'line_num'			=> $i,
				'kode_produk'		=> $row['kode_produk'],
				'nama_produk'		=> $row['nama_produk'],
				'quantity'			=> $row['quantity'],
				'order_quantity'	=> $row['order_quantity'],
				'harga'				=> $row['harga'],
				'line_total'		=> $row['line_total']	
			);
			$i++; 
		}

		//header data
		$data_header = array(
			'no_purchase'			=> $data['no_purchase'],
			'id_supplier'			=> $data['id_supplier'],
			'status'				=> 'D',
			'tanggal_purchase'		=> isset($data['tanggal_purchase']) ? date('Y-m-d', strtotime($data['tanggal_purchase'])) : '',
			'note'					=> $data['note'],
			'subtotal'				=> number_unformat($data['subtotal']),
			'tax_rate'				=> number_unformat($data['tax_rate']),
			'tax'					=> number_unformat($data['tax']),
			'other'					=> number_unformat($data['other']),
			'total'					=> number_unformat($data['total'])
		);

		$query = $this->Purchase_model->addData($data_header, $data_detail);
		
		if($query)
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
		else
			$this->session->set_flashdata('error', 'Data gagal disimpan');
		
		redirect('Purchase');

	}

	function edit($id = 0){	
		if(!$this->Purchase_model->checkID($id)){
			$this->session->set_flashdata('error', 'Data tidak ditemukan');
			redirect('Purchase');
		};

	   	$data = array(
	   		'active'		=> 'Purchasing',
	   		'content'		=> 'Purchase/form_purchase',
	   		'title'			=> 'Edit Purchase',
	   		'page_action' 	=> base_url('Purchase/update_process'),
	   		'supplier'		=> 	$this->Purchase_model->getSupplier(),
	   		'get_data'		=> $this->Purchase_model->edit($id),
	   		'get_data_detail'	=> $this->Purchase_model->editdetail($id)
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	function printdata($id = 0){	
		if(!$this->Purchase_model->checkID($id)){
			$this->session->set_flashdata('error', 'Data tidak ditemukan');
			redirect('Purchase');
		};

		$get_data = $this->Purchase_model->getPrintDataHeader($id);
	   	$data = array(
	   		'active'		=> 'Purchasing',
	   		'print_page'	=> 'Print Page',
	   		'content'		=> 'Purchase/form_purchase_print',
	   		'title'			=> 'Form Purchase',
	   		'page_action' 	=> base_url('Purchase/update_process'),
	   		'supplier'		=> 	$this->Purchase_model->getSupplier(),
	   		'get_data'		=> $get_data,
	   		'poD'	=> $this->Purchase_model->editdetail($id)
		);

	   	// $this->template->load('tpl', $data['content'], $data);

	   	$html = $this->load->view($data['content'], $data, TRUE);

		$filename = "PO_" . $get_data->row()->no_purchase;
		$filename .= "_" . date('dmY');
		$filename .= ".pdf";
		
		$this->load->library('dompdf_gen');
		
		// Convert to PDF
		$this->dompdf->load_html($html);
		$this->dompdf->set_paper("A4"); 
		$this->dompdf->render();
		$this->dompdf->stream($filename);	   	
	}

	function update_process(){
		
		$data 			= $this->fungsi->accept_data(array_keys($_POST));
		$idPurchase		= $data['id_purchase'];

		//detail data
		$order = $data['order'];
		$i = 0;
		foreach($order as $row)
		{
			$data_detail[] = array(
				'line_num'					=> $i,
				'kode_produk'				=> $row['kode_produk'],
				'nama_produk'				=> $row['nama_produk'],
				'quantity'					=> $row['quantity'],
				'order_quantity'			=> $row['order_quantity'],
				'harga'						=> $row['harga'],
				'line_total'				=> $row['line_total'],
				'id_purchase_detail'		=> isset($row['id_purchase_detail']) ? $row['id_purchase_detail'] : 0,	
			);
			$i++; 
		}

		//header data
		$data_header = array(
			'no_purchase'			=> $data['no_purchase'],
			'id_supplier'			=> $data['id_supplier'],
			'status'				=> $data['status'],
			'tanggal_purchase'		=> isset($data['tanggal_purchase']) ? date('Y-m-d', strtotime($data['tanggal_purchase'])) : '',
			'note'					=> $data['note'],
			'subtotal'				=> number_unformat($data['subtotal']),
			'tax_rate'				=> number_unformat($data['tax_rate']),
			'tax'					=> number_unformat($data['tax']),
			'other'					=> number_unformat($data['other']),
			'total'					=> number_unformat($data['total'])
		);

		$query = $this->Purchase_model->updateData($data_header, $data_detail, $idPurchase);
		
		if($query)
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
		else
			$this->session->set_flashdata('error', 'Data gagal disimpan');
		
		redirect('Purchase');

	}

	function searchItem($itemCode=''){ 
		$itemCode = urldecode($itemCode);
		$retData  = $this->Purchase_model->search_item($itemCode);
		
		echo json_encode($retData);
	}
}