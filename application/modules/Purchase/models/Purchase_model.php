<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Purchase_model extends CI_Model 
{    
	var $table = 'purchase_header';
	var $primary = 'id_purchase';

    public function __construct(){
        parent::__construct(); 
    }

    public function getList(){
        $query = $this->db->query("SELECT a.*,
                                    CASE WHEN a.status = 'D' THEN 'Draft' WHEN a.status = 'O' THEN 'Open' WHEN a.status = 'C' THEN 'Close' ELSE '' END AS 'statuspurchase',
                                    b.nama_supplier
                                    FROM purchase_header a
                                    LEFT JOIN supplier b ON a.id_supplier = b.id_supplier
                                    order by a.id_purchase desc");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    public function getListReport($parameter='', $isExcel = FALSE){

        $where  = '';
        $where .= ($parameter['start_date'] != '' && $parameter['end_date'] != '') ? " AND a.tanggal_purchase between '".date('Y-m-d',strtotime($parameter['start_date']))."' AND '".date('Y-m-d',strtotime($parameter['end_date']))."'" : "";
        $where .= ($parameter['id_supplier'] != '') ? " AND a.id_supplier = '".$parameter['id_supplier']."'" : "";
        $where = (trim(substr($where,0,4)) == "AND") ? "WHERE ".substr($where,4,strlen($where)) : $where;

        if($isExcel){
            $query = $this->db->query("SELECT a.no_purchase, b.nama_supplier, a.tanggal_purchase, a.note, a.subtotal, a.tax_rate, a.tax, a.other, a.total,
                                    CASE WHEN a.status = 'D' THEN 'Draft' WHEN a.status = 'O' THEN 'Open' ELSE '' END AS 'statuspurchase'
                                    FROM purchase_header a
                                    LEFT JOIN supplier b ON a.id_supplier = b.id_supplier
                                    " .$where. "
                                    order by a.id_purchase asc");
            if($query->num_rows()>0){
                return $query;
            } else {
                $query->free_result();
                return $query;
            }
        }else{
            $query = $this->db->query("SELECT a.*,
                                        CASE WHEN a.status = 'D' THEN 'Draft' WHEN a.status = 'O' THEN 'Open' ELSE '' END AS 'statuspurchase',
                                        b.nama_supplier
                                        FROM purchase_header a
                                        LEFT JOIN supplier b ON a.id_supplier = b.id_supplier
                                        " .$where. "
                                        order by a.id_purchase desc");
            if($query->num_rows()>0){
                return $query;
            } else {
                $query->free_result();
                return $query;
            }
        }
    }

    function addData($data_header = '', $data_detail = ''){
    
        $this->db->trans_start();
        
        // Insert Into purchase_header Start
        $this->db->set('created_user', $this->session->userdata('user_id'));
        $this->db->set('created_date', 'now()', FALSE);
        $this->db->set($data_header);
        
        $query = $this->db->insert('purchase_header');
        if(!$query) return false;
        $ID = $this->db->insert_id();
        // Insert Into purchase_header End
        
        // Insert Into purchase_detail Start
        for($i = 0; $i < count($data_detail); $i++){
            $this->db->set('id_purchase', $ID, FALSE);
            $this->db->set('created_user', $this->session->userdata('user_id'));
            $this->db->set('created_date', 'now()', FALSE);
            $this->db->set($data_detail[$i]);
            
            $query = $this->db->insert('purchase_detail');
            if(!$query) return false;
        }
        // Insert Into purchase_detail End
        
        $this->db->trans_complete();
        
        return true;
    }

    function updateData($data_header = '', $data_detail = '', $idpurchase = 0){

        $this->db->trans_start();

        // Update Into purchase_header Start
        $this->db->set('updated_user', $this->session->userdata('user_id'), FALSE);
        $this->db->set('updated_date', 'now()', FALSE);
        $this->db->set($data_header); 
        $this->db->where('id_purchase', $idpurchase); 
        $query = $this->db->update('purchase_header');
        if(!$query) return false;
        // Update Into purchase_header End

        // Update Into purchase detail Start
        $itd    = '';
        foreach($data_detail as $row) {
            $itDetailID = $row['id_purchase_detail'];
            unset($row['id_purchase_detail']);
            
            if ($itDetailID > 0) { 
                
                $itd    .= ($itd == '') ? $itDetailID : ','.$itDetailID; 
                $this->db->set('id_purchase', $idpurchase, FALSE);
                $this->db->set('updated_user', $this->session->userdata('user_id'));
                $this->db->set('updated_date', 'now()', FALSE);
                $this->db->set($row);
                $this->db->where('id_purchase_detail', $itDetailID); 
                $query = $this->db->update('purchase_detail'); 

            }else { 
                
                $this->db->set('id_purchase', $idpurchase, FALSE);
                $this->db->set('created_user', $this->session->userdata('user_id'));
                $this->db->set('created_date', 'now()', FALSE);
                $this->db->set($row);
                $query = $this->db->insert('purchase_detail'); 
                $ID = $this->db->insert_id();
                $itd    .= ($itd == '') ? $ID : ','.$ID; 

            } 
        }
        // Update Into purchase_detail End

        if ($itd != '') {
            $this->db->query("DELETE FROM purchase_detail where id_purchase = ? and id_purchase_detail not in (".$itd.")", $idpurchase);
        }

        //insert stok start
        if ($data_header['status'] == 'O')
        {
            // die(var_dump($data_detail));
            foreach($data_detail as $row) {

                // stok
                $check_stock = $this->db->query("SELECT * FROM stock WHERE kode_produk = '".$row['kode_produk']."' ");
                if($check_stock->num_rows() > 0){

                    $total = $check_stock->row_object()->quantity + ($row['quantity'] * $row['order_quantity']);

                    $this->db->set('quantity', $total);
                    $this->db->set('updated_user', $this->session->userdata('user_id'));
                    $this->db->set('updated_date', 'now()', FALSE);
                    $this->db->where('kode_produk', $row['kode_produk']);
                    $simpanstock = $this->db->update('stock');
                    if(!$simpanstock) return false;

                } else {

                    $this->db->set('kode_produk', $row['kode_produk']);
                    $this->db->set('quantity', ($row['quantity'] * $row['order_quantity']));
                    $this->db->set('created_user', $this->session->userdata('user_id'));
                    $this->db->set('created_date', 'now()', FALSE);
                    $simpanstocknew = $this->db->insert('stock');
                    if(!$simpanstocknew) return false;

                }

                // stok opname
                $this->db->set('no_purchase', $data_header['no_purchase']);
                $this->db->set('id_supplier', $data_header['id_supplier']);
                $this->db->set('tanggal_transaksi', $data_header['tanggal_purchase']);
                $this->db->set('kode_produk', $row['kode_produk']);
                $this->db->set('quantity_masuk', ($row['quantity'] * $row['order_quantity']));
                $this->db->set('created_user', $this->session->userdata('user_id'));
                $this->db->set('created_date', 'now()', FALSE);
                $simpanstocknew = $this->db->insert('stock_opname');
                if(!$simpanstocknew) return false;

            }
        }
        //insert stok end

        $this->db->trans_complete(); 

        return true;
    }

    function checkID($id){
        $query = $this->db->get_where($this->table, array($this->primary=>$id));
        if ($query->num_rows() > 0) 
            return true;
        else
            return false;
    }

    public function getPrintDataHeader($id){
        $sql = "SELECT ph.*, sp.*,
                CASE WHEN ph.status = 'D' THEN 'Draft' WHEN ph.status = 'O' THEN 'Open' WHEN ph.status = 'C' THEN 'Close' ELSE '' END 
                AS 'statuspurchase' 
                FROM purchase_header ph
                INNER JOIN supplier sp ON ph.id_supplier=sp.id_supplier
                WHERE ph.id_purchase = '".$id."'";

        $query = $this->db->query($sql);
        
        if($query->num_rows()>0){
            return $query;
        }else{
            $query->free_result();
            return $query;
        }
    }

    public function edit($id){
        $query = $this->db->query("SELECT *, CASE WHEN status = 'D' THEN 'Draft' WHEN status = 'O' THEN 'Open' WHEN status = 'C' THEN 'Close' ELSE '' END AS 'statuspurchase' FROM purchase_header WHERE id_purchase = '".$id."'");
        return $query;
    }

    public function editdetail($id){
        $query = $this->db->query("SELECT *
                                    FROM purchase_detail
                                    WHERE id_purchase = '".$id."'
                                    order by line_num ");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    function search_item($id=''){

    	$ret = '';  
    	$sql = "
    			SELECT a.kode_produk, a.nama_produk, b.harga
    			FROM produk a
                inner join harga b on a.kode_produk = b.kode_produk
    			WHERE (a.kode_produk like '%".$id."%' or a.nama_produk like '%".$id."%') and a.aktif = 'Y'
    			";

    	$q = $this->db->query($sql);

    	if ($q->num_rows() > 0) {
    		foreach($q->result() as $row) {
                $ret[] = array("kode_produk" 	=> $row->kode_produk, 
                               "nama_produk" 	=> $row->nama_produk,
                               "harga"          => $row->harga);
            }
    	}

    	return $ret;

    }

    function getSupplier(){
        $query = $this->db->query("SELECT id_supplier, nama_supplier from supplier where aktif = 'Y' order by nama_supplier asc");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }
}   
