        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <?php echo $this->load->view('alert');?>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search"></div>
              </div>
            </div>

            <div class="clearfix"></div>

            <!-- start filter data -->
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filter Purchase </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left" method="POST" action="<?php echo site_url('Purchase/report'); ?>" >

                      <!-- start left side -->
                      <div class="col-md-6">
                        <div class="form-group form-inline">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Purchase</label>
                          <div class="col-md-9 col-sm-9 col-xs-12">
                            <input type="text" class="form-control has-feedback-left datepicker" id="start_date" name="start_date" aria-describedby="inputSuccess2Status3" value="<?php echo !empty($param['start_date']) ? date('d-m-Y', strtotime($param['start_date'])) : date('d-m-Y');?>" data-date-format="dd-mm-yyyy"> - 
                            <input type="text" class="form-control has-feedback-left datepicker" id="end_date" name="end_date" aria-describedby="inputSuccess2Status3" value="<?php echo !empty($param['end_date']) ? date('d-m-Y', strtotime($param['end_date'])) : date('d-m-Y');?>" data-date-format="dd-mm-yyyy">
                          </div>
                        </div>

                      </div>
                      <!-- end left side -->

                      <!-- start right side -->
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Supplier</label>
                          <div class="col-md-9 col-sm-9 col-xs-12">
                            <select class="form-control select2_single" name="id_supplier">
                              <option value="">-- Select Supplier --</option>
                              <?php foreach ($supplier->result() as $baris) {
                                if (!empty($param['id_supplier']) && $baris->id_supplier == $param['id_supplier']) { ?>
                                <option value="<?php echo $baris->id_supplier; ?>" selected><?php echo strtoupper($baris->nama_supplier); ?></option>
                                 <?php } else { ?>
                                <option value="<?php echo $baris->id_supplier; ?>"><?php echo strtoupper($baris->nama_supplier); ?></option>
                              <?php } }?>
                            </select>
                          </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                          <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                            <button type="submit" class="btn btn-primary" name="submit" value="findData"><i class="fa fa-search"></i> Cari data </button> &nbsp; &nbsp;
                            <button class="btn btn-warning" id="submit" name="submit" value="exportToExcel"><i class="fa fa-print icon-white"></i> Export to Excel</button> 
                          </div>
                        </div>
                      </div>
                      <!-- end right side -->

                    </form>
                  </div>
                </div>
              </div>
            </div>
            <!-- end filter data -->

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title; ?> </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>No.</th>
                          <th>No. Document</th>
                          <th>Status</th>
                          <th>Tanggal</th>
                          <th>Supplier</th>
                          <th>Action</th>
                        </tr>
                      </thead>

                      <tbody>
                        <?php
                        $no = 1;
                        if ($list != '') :
                        foreach($list->result() as $row){ 
                        ?>
                        <tr>
                          <td><?php echo $no++; ?></td>
                          <td><?php echo $row->no_purchase; ?></td>
                          <td><?php echo $row->statuspurchase; ?></td>
                          <td><?php echo date('d-m-Y',strtotime($row->tanggal_purchase)); ?></td>
                          <td><?php echo $row->nama_supplier; ?></td>
                          <td>
                            <a class="glyphicon glyphicon-edit" href="<?php echo base_url();?>Purchase/edit/<?php echo $row->id_purchase;?>" title="Edit"></a>
                          </td>
                        </tr>
                        <?php } endif;?>
                      </tbody>
                    </table>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->