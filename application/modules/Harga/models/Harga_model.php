<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Harga_model extends CI_Model 
{    
	var $table = 'harga';
	var $primary = 'id_harga';

    public function __construct(){
        parent::__construct(); 
    }

    public function getList(){
        $query = $this->db->query("SELECT T0.*, 
        							T1.nama_produk,
        							CASE T0.aktif WHEN 'Y' THEN 'YA' ELSE 'TIDAK' END AS status_aktif 
        							FROM harga T0
        							LEFT JOIN produk T1 ON T0.kode_produk = T1.kode_produk
        							order by T0.kode_produk asc");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    public function save($id ='', $data =''){

    	if(!empty($data)){
    		$this->db->where($this->primary, $id);
			$is_check = $this->db->get($this->table);

			if($is_check->num_rows() > 0){

				$this->db->set($data);
				$this->db->set('updated_user', $this->session->userdata('user_id'));
				$this->db->set('updated_date', 'now()', FALSE);
				$this->db->where($this->primary, $id);
				
				$query = $this->db->update($this->table);
				if($query)
					return true;
				else
					return false;
			} else {
				$this->db->set('created_user', $this->session->userdata('user_id'));
				$this->db->set('created_date', 'now()', FALSE);
				$query = $this->db->insert($this->table, $data);
				if($query)
					return true;
				else
					return false;
			}
    	}  	
    }

    public function delete($id){
		$query = $this->db->delete($this->table,array($this->primary=>$id));
		if($query)
			return true;
		else
			return false;
	}

	function checkID($id){
		$query = $this->db->get_where($this->table, array($this->primary=>$id));
		if ($query->num_rows() > 0) 
			return true;
		else
			return false;
    }

    public function edit($id){
		$query = $this->db->get_where($this->table, array($this->primary=>$id));
		return $query;
	}

	function getProduk(){
        $query = $this->db->query("SELECT kode_produk, nama_produk from produk where aktif = 'Y' order by kode_produk asc");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    function cekDuplicateCode($product = '', $id = ''){
		
		if($id != '')
			$query = $this->db->query("SELECT * FROM harga WHERE kode_produk = '" . $product . "' AND id_harga <> '" . $id . "'");
		else
			$query = $this->db->query("SELECT * FROM harga WHERE kode_produk = '" . $product . "' ");
		
		if($query->num_rows() > 0){
			return false;
		}else{
			return true;	
		}
	}

}   
