<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Harga extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Harga_model');
		if(!is_logged_in()){redirect('Inadminpage');}	
		// if(!is_access()){
		// 	$this->session->set_flashdata('error', 'Anda tidak berhak mengakses halaman ini');
		// 	redirect('Home');
		// }
	}

	public function index(){	
		
	   	$data = array(
	   		'active'	=> 'Master',
	   		'content'	=> 'Harga/list_harga',
	   		'title'		=> 'Harga',
	   		'list'		=> $this->Harga_model->getList()
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	public function add(){	
		
	   	$data = array(
	   		'active'		=> 'Master',
	   		'content'		=> 'Harga/form_harga',
		   	'title'			=> 'New Harga',
		   	'produk'		=> 	$this->Harga_model->getProduk(),
	   		'page_action' 	=> base_url('Harga/save'),
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	

	function save(){
		
		$this->form_validation->set_rules('kode_produk','Produk', 'trim|required');
		$this->form_validation->set_rules('harga','Harga', 'trim|required');

		$accept = $this->fungsi->accept_data(array_keys($_POST));

        $this->form_validation->set_rules('kode_produk','Kode Produk', 'trim|required|callback_checkDuplicate');	

		if ($this->form_validation->run() == FALSE) { 
			
			$this->session->set_flashdata('error', validation_errors());
			redirect('Harga');

		} else {
			
			$data = array(
						'kode_produk'			=> $accept['kode_produk'],
						'harga'					=> number_unformat($accept['harga']),
						'aktif'					=> $accept['aktif']);

			$query = $this->Harga_model->save($accept['id_harga'], $data);

			if($query)
				$this->session->set_flashdata('success', 'Data berhasil disimpan');
			else
				$this->session->set_flashdata('error', 'Data gagal disimpan');
			
			redirect('Harga');
		}	
	}

	function checkDuplicate($product = '') { 

		$id = $this->input->post('id_harga');
		$check = $this->Harga_model->cekDuplicateCode($product, $id);
		$this->form_validation->set_message('checkDuplicate', 'Data harga untuk kode produk : '.$product.' sudah ada');
	
		if($check){
			return true;
		}else{
			return false;	
		}

	}

	public function delete($id){
		if (!$id) redirect('Harga');	
		$query = $this->Harga_model->delete($id);
		if($query)
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
		else
			$this->session->set_flashdata('error', 'Data gagal dihapus');
		
		redirect('Harga');
	}

	function edit($id = 0){	
		if(!$this->Harga_model->checkID($id)){
			$this->session->set_flashdata('error', 'Data tidak ditemukan');
			redirect('Harga');
		};

	   	$data = array(
	   		'active'		=> 'Master',
	   		'content'		=> 'Harga/form_harga',
	   		'title'			=> 'Edit Harga',
	   		'page_action' 	=> base_url('Harga/save'),
	   		'produk'		=> $this->Harga_model->getProduk(),
	   		'get_data'		=> $this->Harga_model->edit($id),
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}
}