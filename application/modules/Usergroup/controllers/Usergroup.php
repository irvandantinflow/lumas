<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usergroup extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Usergroup_model');
		if(!is_logged_in()){redirect('Inadminpage');}
		// if(!is_access()){
		// 	$this->session->set_flashdata('error', 'Anda tidak berhak mengakses halaman ini');
		// 	redirect('Home');
		// }	
	}

	public function index(){	
		
	   	$data = array(
	   		'active'	=> 'Master',
	   		'content'	=> 'Usergroup/list_usergroup',
	   		'list'		=> $this->Usergroup_model->getList()
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	public function add(){	
		
	   	$data = array(
	   		'active'		=> 'Master',
	   		'content'		=> 'Usergroup/form_usergroup',
		   	'title'			=> 'New Usergroup',
	   		'page_action' 	=> base_url('Usergroup/save'),
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	function save(){
		$this->form_validation->set_rules('usergroup','Kode Usergroup', 'trim|required');

		$accept = $this->fungsi->accept_data(array_keys($_POST));

		if ($this->form_validation->run() == FALSE) { 
			
			$this->session->set_flashdata('error', validation_errors());
			redirect('Usergroup');

		} else {

			$data = array(
				'usergroup'			=> $accept['usergroup'],
				'aktif'				=> isset($accept['aktif']) ? $accept['aktif'] : 'N'
			);

			$query = $this->Usergroup_model->save($accept['id_usergroup'], $data);

			if($query)
				$this->session->set_flashdata('success', 'Data berhasil disimpan');
			else
				$this->session->set_flashdata('error', 'Data gagal disimpan');
			
			redirect('Usergroup');
		}	
	}

	public function delete($id){
		if (!$id) redirect('Usergroup');	
		$query = $this->Usergroup_model->delete($id);
		if($query)
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
		else
			$this->session->set_flashdata('error', 'Data gagal dihapus');
		
		redirect('Usergroup');
	}

	function edit($id = 0){	
		if(!$this->Usergroup_model->checkID($id)){
			$this->session->set_flashdata('error', 'Data tidak ditemukan');
			redirect('Usergroup');
		};

	   	$data = array(
	   		'active'		=> 'Master',
	   		'content'		=> 'Usergroup/form_usergroup',
	   		'title'			=> 'Edit Usergroup',
	   		'page_action' 	=> base_url('Usergroup/save'),
	   		'get_data'		=> $this->Usergroup_model->edit($id),
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}
}