<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stock extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Stock_model');
		if(!is_logged_in()){redirect('Inadminpage');}	
		// if(!is_access()){
		// 	$this->session->set_flashdata('error', 'Anda tidak berhak mengakses halaman ini');
		// 	redirect('Home');
		// }
	}

	public function index(){	
		
	   	$data = array(
	   		'active'	=> 'Inventory',
	   		'content'	=> 'Stock/list_stock',
	   		'title'		=> 'Stock',
	   		'list'		=> $this->Stock_model->getList()
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	public function inventory(){	
		
	   	$data = array(
	   		'active'	=> 'Report',
	   		'content'	=> 'Stock/list_stock',
	   		'title'		=> 'Stock',
	   		'list'		=> $this->Stock_model->getList()
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	public function printdata(){	
		
	 //   	$data = array(
	 //   		'active'		=> 'Inventory',
	 //   		'content'		=> 'Stock/list_stock_print',
	 //   		'title'			=> 'Laporan Stock',
	 //   		'print_page'	=> 'Print Page',
	 //   		'list'			=> $this->Stock_model->getList()
		// );

		// $this->template->load('tpl', $data['content'], $data);
		$this->load->library('to_excel');
		
		
		$title	 = '<div style="font-size: 24px"><strong>PT LUMAS JAYA INDUSTRI</strong></div>';
		$title	.= '<div style="font-size: 18px"><strong>Laporan Stock</strong></div><br />';
		$title	.= '<strong>Tanggal Cetak Report: ' . date('d-m-Y') . '</strong><br />';
		$title	.= '<br />';
		
		$dataRpt = $this->Stock_model->getList(TRUE)->result_array();
		to_excel($dataRpt,
			'Stock_Report-' . date('dmY'),
			$title
		);

	}
}