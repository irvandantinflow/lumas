<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stock_model extends CI_Model 
{

    public function __construct(){
        parent::__construct(); 
    }

    public function getList($isExcel = FALSE){
        if($isExcel){
            $query = $this->db->query("SELECT T0.kode_produk, 
                                        T1.nama_produk, T0.quantity, CASE WHEN T0.quantity < 1000 THEN 'HARAP SEGERA MEMESAN BARANG' ELSE '' END AS note
                                        FROM stock T0
                                        LEFT JOIN produk T1 ON T0.kode_produk = T1.kode_produk
                                        order by T0.kode_produk asc");
            if($query->num_rows()>0){
                return $query;
            } else {
                $query->free_result();
                return $query;
            }
        }else{
            $query = $this->db->query("SELECT T0.*, 
            							T1.nama_produk
            							FROM stock T0
            							LEFT JOIN produk T1 ON T0.kode_produk = T1.kode_produk
            							order by T0.kode_produk asc");
            if($query->num_rows()>0){
                return $query;
            } else {
                $query->free_result();
                return $query;
            }
        }
    }
}
