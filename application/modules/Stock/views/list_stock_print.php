<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <?php echo $this->load->view('alert');?>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search"></div>
      </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?php echo $title; ?> </h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Kode Produk</th>
                  <th>Nama Produk</th>
                  <th>Quantity</th>
                  <th>Note</th>
                </tr>
              </thead>
              <tbody>
              	<?php
                $no = 1;
                if ($list != '') :
		            foreach($list->result() as $row){ 
                ?>
                <tr style="background: <?php echo ($row->quantity < 1000) ? 'rgba(17, 246, 238, 0.16)' : ''; ?>">
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $row->kode_produk; ?></td>
                  <td><?php echo $row->nama_produk; ?></td>
                  <td><?php echo $row->quantity; ?></td>
                  <td><?php echo ($row->quantity < 1000) ? 'HARAP SEGERA MEMESAN BARANG' : ''; ?></td>
                </tr>
                <?php } endif;?>
              </tbody>
            </table>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->