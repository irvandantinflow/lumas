<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Supplier_model');
		if(!is_logged_in()){redirect('Inadminpage');}	
		// if(!is_access()){
		// 	$this->session->set_flashdata('error', 'Anda tidak berhak mengakses halaman ini');
		// 	redirect('Home');
		// }
	}

	public function index(){	
		
	   	$data = array(
	   		'active'	=> 'Purchasing',
	   		'content'	=> 'Supplier/list_supplier',
	   		'title'		=> 'Supplier',
	   		'list'		=> $this->Supplier_model->getList()
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	public function add(){	
		
	   	$data = array(
	   		'active'		=> 'Purchasing',
	   		'content'		=> 'Supplier/form_supplier',
		   	'title'			=> 'New Supplier',
	   		'page_action' 	=> base_url('Supplier/save'),
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	function save(){
		
		$this->form_validation->set_rules('nama_supplier','Nama Supplier', 'trim|required');

		$accept = $this->fungsi->accept_data(array_keys($_POST));

		if ($this->form_validation->run() == FALSE) { 
			
			$this->session->set_flashdata('error', validation_errors());
			redirect('Supplier');

		} else {
			
			$data = array(
						'nama_supplier'				=> $accept['nama_supplier'],
						'telp_supplier'				=> $accept['telp_supplier'],
						'alamat_supplier'			=> $accept['alamat_supplier'],
						'aktif'						=> $accept['aktif']);

			$query = $this->Supplier_model->save($accept['id_supplier'], $data);

			if($query)
				$this->session->set_flashdata('success', 'Data berhasil disimpan');
			else
				$this->session->set_flashdata('error', 'Data gagal disimpan');
			
			redirect('Supplier');
		}	
	}

	public function delete($id){
		if (!$id) redirect('Supplier');	
		$query = $this->Supplier_model->delete($id);
		if($query)
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
		else
			$this->session->set_flashdata('error', 'Data gagal dihapus');
		
		redirect('Supplier');
	}

	function edit($id = 0){	
		if(!$this->Supplier_model->checkID($id)){
			$this->session->set_flashdata('error', 'Data tidak ditemukan');
			redirect('Supplier');
		};

	   	$data = array(
	   		'active'		=> 'Purchasing',
	   		'content'		=> 'Supplier/form_supplier',
	   		'title'			=> 'Edit Supplier',
	   		'get_data'		=> $this->Supplier_model->edit($id),
	   		'page_action' 	=> base_url('Supplier/save')
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}
}