<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Deliveryorder extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Deliveryorder_model');
		if(!is_logged_in()){redirect('Inadminpage');}	
		// if(!is_access()){
		// 	$this->session->set_flashdata('error', 'Anda tidak berhak mengakses halaman ini');
		// 	redirect('Home');
		// }
	}

	public function index(){	
		
	   	$data = array(
	   		'active'	=> 'Inventory',
	   		'content'	=> 'Deliveryorder/list_deliveryorder',
	   		'title'		=> 'Delivery Order',
	   		'list'		=> $this->Deliveryorder_model->getList()
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	public function add($id=0){	
		if($id){
			if(!$this->Deliveryorder_model->checkSOByID($id)){
				$this->session->set_flashdata('error', 'No. Sales Order Tidak ditemukan.');
    			redirect('Deliveryorder'); 
			}
			if(!$this->Deliveryorder_model->checkSOStatusByID($id)) {
                $this->session->set_flashdata('error', 'Status Sales Order Sudah Close.');
    			redirect('Deliveryorder'); 
            } 
		}
	   	$data = array(
	   		'active'		=> 'Inventory',
	   		'content'		=> 'Deliveryorder/form_deliveryorder',
		   	'title'			=> 'New Delivery Order',
		   	'customer'		=> 	$this->Deliveryorder_model->getCustomerbySOID($id),
		   	'kurir'			=> $this->Deliveryorder_model->getKurir(),
		   	'page_action' 	=> base_url('Deliveryorder/add_process'),
	   		'get_dataDOH'		=> $this->Deliveryorder_model->getSOH($id),
	   		'get_data_detail'	=> $this->Deliveryorder_model->getSOD($id),
	   		//'get_dataDOH'	=> ''	   	
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	function add_process(){
		
		$data 	= $this->fungsi->accept_data(array_keys($_POST));

		//detail data
		$order = $data['order'];
		$i = 0;
		foreach($order as $row)
		{
			$data_detail[] = array(
				'line_num'			=> $i,
				'kode_produk'		=> $row['kode_produk'],
				'nama_produk'		=> $row['nama_produk'],
				'quantity'			=> $row['quantity'],
				'order_quantity'	=> $row['order_quantity'],
				'harga'				=> $row['harga'],
				'line_total'		=> $row['line_total']	
			);
			$i++; 
		}

		//header data
		$data_header = array(
			'no_deliveryorder'			=> $data['no_deliveryorder'],
			'id_customer'			=> $data['id_customer'],
			'status'				=> 'O',
			'tanggal_deliveryorder'		=> isset($data['tanggal_deliveryorder']) ? date('Y-m-d', strtotime($data['tanggal_deliveryorder'])) : '',
			'notedo'					=> $data['notedo'],
			'subtotal'				=> number_unformat($data['subtotal']),
			'tax_rate'				=> number_unformat($data['tax_rate']),
			'tax'					=> number_unformat($data['tax']),
			'termofpayment'					=> $data['termofpayment'],
			'total'					=> number_unformat($data['total']),
			'id_salesorder'			=> $data['id_salesorder'],
			'id_kurir'				=> $data['id_kurir']
		);

		$query = $this->Deliveryorder_model->addData($data_header, $data_detail);
		
		if($query)
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
		else
			$this->session->set_flashdata('error', 'Data gagal disimpan');
		
		redirect('Deliveryorder');

	}

	function edit($id = 0){	
		if(!$this->Deliveryorder_model->checkID($id)){
			$this->session->set_flashdata('error', 'Data tidak ditemukan');
			redirect('Deliveryorder');
		};

	   	$data = array(
	   		'active'		=> 'Inventory',
	   		'content'		=> 'deliveryorder/form_deliveryorder',
	   		'title'			=> 'Edit Delivery order',
	   		'page_action' 	=> base_url('deliveryorder/update_process'),
	   		'customer'		=> 	$this->Deliveryorder_model->getCustomerbyDOID($id),
	   		'get_dataDOH'		=> $this->Deliveryorder_model->edit($id),
	   		'get_data_detail'	=> $this->Deliveryorder_model->editdetail($id)
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	function update_process(){
		
		$data 			= $this->fungsi->accept_data(array_keys($_POST));
		$iddeliveryorder		= $data['id_deliveryorder'];

		//detail data
		$order = $data['order'];
		$i = 0;
		foreach($order as $row)
		{
			$data_detail[] = array(
				'line_num'					=> $i,
				'kode_produk'				=> $row['kode_produk'],
				'nama_produk'				=> $row['nama_produk'],
				'quantity'					=> $row['quantity'],
				'order_quantity'			=> $row['order_quantity'],
				'harga'						=> $row['harga'],
				'line_total'				=> $row['line_total'],
				'id_deliveryorder_detail'		=> isset($row['id_deliveryorder_detail']) ? $row['id_deliveryorder_detail'] : 0,	
			);
			$i++; 
		}

		//header data
		$data_header = array(
			'no_deliveryorder'			=> $data['no_deliveryorder'],
			'id_customer'			=> $data['id_customer'],
			'status'				=> $data['status'],
			'tanggal_deliveryorder'		=> isset($data['tanggal_deliveryorder']) ? date('Y-m-d', strtotime($data['tanggal_deliveryorder'])) : '',
			'note'					=> $data['note'],
			'subtotal'				=> number_unformat($data['subtotal']),
			'tax_rate'				=> number_unformat($data['tax_rate']),
			'tax'					=> number_unformat($data['tax']),
			'termofpayment'					=> $data['termofpayment'],
			'total'					=> number_unformat($data['total'])
		);

		$query = $this->Deliveryorder_model->updateData($data_header, $data_detail, $iddeliveryorder);
		
		if($query)
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
		else
			$this->session->set_flashdata('error', 'Data gagal disimpan');
		
		redirect('Deliveryorder');

	}

	function searchItem($itemCode=''){ 
		$itemCode = urldecode($itemCode);
		$retData  = $this->Deliveryorder_model->search_item($itemCode);
		
		echo json_encode($retData);
	}

	function printdata($id = 0){	
		if(!$this->Deliveryorder_model->checkID($id)){
			$this->session->set_flashdata('error', 'Data tidak ditemukan');
			redirect('Purchase');
		};

		$get_data = $this->Deliveryorder_model->getPrintDataHeader($id);
	   	$data = array(
	   		'active'		=> 'Inventory',
	   		'print_page'	=> 'Print Page',
	   		'content'		=> 'deliveryorder/form_deliveryorder_print',
	   		'title'			=> 'Form Delivery Order',
	   		'page_action' 	=> base_url('deliveryorder/update_process'),
	   		'get_data'		=> $get_data,
	   		'soD'	=> $this->Deliveryorder_model->editdetail($id)
		);

		// $this->load->view($data['content'], $data);
	   	$html = $this->load->view($data['content'], $data, TRUE);

		$filename = "DO_" . $get_data->row()->no_deliveryorder;
		$filename .= "_" . date('dmY');
		$filename .= ".pdf";
		
		$this->load->library('dompdf_gen');
		
		// Convert to PDF
		$this->dompdf->load_html($html);
		$this->dompdf->set_paper("A4"); 
		$this->dompdf->render();
		$this->dompdf->stream($filename);	   	
	}	
}