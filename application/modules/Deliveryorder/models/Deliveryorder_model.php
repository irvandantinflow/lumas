<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Deliveryorder_model extends CI_Model 
{    
	var $table = 'deliveryorder_header';
	var $primary = 'id_deliveryorder';

    public function __construct(){
        parent::__construct(); 
    }

    public function getList(){
        $query = $this->db->query("SELECT a.*,
                                    CASE WHEN a.status = 'D' THEN 'Draft' WHEN a.status = 'O' THEN 'Open' ELSE '' END AS 'statusdeliveryorder',
                                    b.nama_customer
                                    FROM deliveryorder_header a
                                    LEFT JOIN customer b ON a.id_customer = b.id_customer
                                    order by a.id_deliveryorder desc");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    public function getSOH($id){
        $query = $this->db->query("SELECT *, status as statusSO, 'Open' AS 'statusdeliveryorder' FROM salesorder_header WHERE id_salesorder = '".$id."' AND status = 'O'");
        if ($query->num_rows() > 0){ 
            return $query;
        }else{
            $query->free_result();
            return $query;  
        }
    }

     public function getSOD($id){
        $query = $this->db->query("SELECT *
                                    FROM salesorder_detail
                                    WHERE id_salesorder = '".$id."'
                                    order by line_num ");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    function addData($data_header = '', $data_detail = ''){
    
        $this->db->trans_start();
        
        $id_salesorder = $data_header['id_salesorder'];

        // Insert Into salesorder_header Start
        $this->db->set('created_user', $this->session->userdata('user_id'));
        $this->db->set('created_date', 'now()', FALSE);
        $this->db->set($data_header);
        
        $query = $this->db->insert('deliveryorder_header');
        if(!$query) return false;
        $ID = $this->db->insert_id();
        // Insert Into salesorder_header End
        
        // Insert Into salesorder_detail Start
        for($i = 0; $i < count($data_detail); $i++){
            $this->db->set('id_deliveryorder', $ID, FALSE);
            $this->db->set('created_user', $this->session->userdata('user_id'));
            $this->db->set('created_date', 'now()', FALSE);
            $this->db->set($data_detail[$i]);
            
            $query = $this->db->insert('deliveryorder_detail');
            if(!$query) return false;
        }
        // Insert Into salesorder_detail End

        $this->db->set('status', 'C');
        $this->db->set('updated_user', $this->session->userdata('user_id'), FALSE);
        $this->db->set('updated_date', 'now()', FALSE);
        $this->db->where('id_salesorder', $id_salesorder);
        $query = $this->db->update('salesorder_header');
        if(!$query) return false;

        $this->db->trans_complete();
        
        return true;
    }

    function updateData($data_header = '', $data_detail = '', $idsalesorder = 0){

        $this->db->trans_start();

        // Update Into salesorder_header Start
        $this->db->set('updated_user', $this->session->userdata('user_id'), FALSE);
        $this->db->set('updated_date', 'now()', FALSE);
        $this->db->set($data_header); 
        $this->db->where('id_salesorder', $idsalesorder); 
        $query = $this->db->update('salesorder_header');
        if(!$query) return false;
        // Update Into salesorder_header End

        // Update Into salesorder detail Start
        $itd    = '';
        foreach($data_detail as $row) {
            $itDetailID = $row['id_salesorder_detail'];
            unset($row['id_salesorder_detail']);
            
            if ($itDetailID > 0) { 
                
                $itd    .= ($itd == '') ? $itDetailID : ','.$itDetailID; 
                $this->db->set('id_salesorder', $idsalesorder, FALSE);
                $this->db->set('updated_user', $this->session->userdata('user_id'));
                $this->db->set('updated_date', 'now()', FALSE);
                $this->db->set($row);
                $this->db->where('id_salesorder_detail', $itDetailID); 
                $query = $this->db->update('salesorder_detail'); 

            }else { 
                
                $this->db->set('id_salesorder', $idsalesorder, FALSE);
                $this->db->set('created_user', $this->session->userdata('user_id'));
                $this->db->set('created_date', 'now()', FALSE);
                $this->db->set($row);
                $query = $this->db->insert('salesorder_detail'); 
                $ID = $this->db->insert_id();
                $itd    .= ($itd == '') ? $ID : ','.$ID; 

            } 
        }
        // Update Into salesorder_detail End

        if ($itd != '') {
            $this->db->query("DELETE FROM salesorder_detail where id_salesorder = ? and id_salesorder_detail not in (".$itd.")", $idsalesorder);
        }

        $this->db->trans_complete(); 

        return true;
    }

    function checkID($id){
        $query = $this->db->get_where($this->table, array($this->primary=>$id));
        if ($query->num_rows() > 0) 
            return true;
        else
            return false;
    }

    function checkSOByID($id){
        $query = $this->db->get_where("salesorder_header", array("id_salesorder"=>$id));
        if ($query->num_rows() > 0) 
            return true;
        else
            return false;        
    }

    function checkSOStatusByID($id){
        $query = $this->db->query("SELECT * FROM `salesorder_header` WHERE id_salesorder = '".$id."' AND status = 'C'");
        if($query->num_rows() > 0)
            return FALSE;
        else
            return true;
    }

    public function edit($id){
        $query = $this->db->query("SELECT do.*, CASE WHEN do.status = 'D' THEN 'Draft' WHEN do.status = 'O' THEN 'Open' 
                    ELSE '' END AS 'statusdeliveryorder', so.no_salesorder, k.* 
                    FROM deliveryorder_header do INNER JOIN salesorder_header so ON do.id_salesorder=so.id_salesorder
                    INNER JOIN kurir k ON k.id_kurir=do.id_kurir
                    WHERE do.id_deliveryorder = '".$id."'");
        return $query;
    }

    public function editdetail($id){
        $query = $this->db->query("SELECT *
                                    FROM deliveryorder_detail
                                    WHERE id_deliveryorder = '".$id."'
                                    order by line_num ");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    function search_item($id=''){

    	$ret = '';  
    	$sql = "
    			SELECT a.kode_produk, a.nama_produk, b.harga
    			FROM produk a
                left join harga b on a.kode_produk = b.kode_produk
    			WHERE (a.kode_produk like '%".$id."%' or a.nama_produk like '%".$id."%') and a.aktif = 'Y'
    			";

    	$q = $this->db->query($sql);

    	if ($q->num_rows() > 0) {
    		foreach($q->result() as $row) {
                $ret[] = array("kode_produk" 	=> $row->kode_produk, 
                               "nama_produk" 	=> $row->nama_produk,
                               "harga"          => $row->harga);
            }
    	}

    	return $ret;

    }

    function getCustomer(){
        $query = $this->db->query("SELECT id_customer, nama_customer from customer where aktif = 'Y' order by nama_customer asc");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    function getCustomerbySOID($id){
        $query = $this->db->query("SELECT c.nama_customer, so.id_customer FROM salesorder_header so 
                                    INNER JOIN customer c ON so.id_customer=c.id_customer 
                                    WHERE so.id_salesorder = '".$id."'");
        if($query->num_rows()>0){
            return $query->first_row();
        } else {
            $query->free_result();
            return $query;
        }
    }

    function getCustomerbyDOID($id){
        $query = $this->db->query("SELECT c.nama_customer, do.id_customer FROM deliveryorder_header do 
                                    INNER JOIN customer c ON do.id_customer=c.id_customer 
                                    WHERE do.id_deliveryorder = '".$id."'");
        if($query->num_rows()>0){
            return $query->first_row();
        } else {
            $query->free_result();
            return $query;
        }
    }

    public function getPrintDataHeader($id){
        $sql = "SELECT so.*, c.*,
                CASE WHEN so.status = 'D' THEN 'Draft' WHEN so.status = 'O' THEN 'Open' WHEN so.status = 'C' THEN 'Close' ELSE '' END 
                AS 'statusdeliveryorder' 
                FROM deliveryorder_header so
                INNER JOIN customer c ON c.id_customer=so.id_customer
                WHERE so.id_deliveryorder = '".$id."'";

        $query = $this->db->query($sql);
        
        if($query->num_rows()>0){
            return $query;
        }else{
            $query->free_result();
            return $query;
        }
    }        

    function getKurir(){
        $query = $this->db->query("SELECT * from kurir where aktif = 'Y' order by nama_kurir asc");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }    
}   
