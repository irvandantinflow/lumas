<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kurir extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Kurir_model');
		if(!is_logged_in()){redirect('Inadminpage');}	
		// if(!is_access()){
		// 	$this->session->set_flashdata('error', 'Anda tidak berhak mengakses halaman ini');
		// 	redirect('Home');
		// }
	}

	public function index(){	
		
	   	$data = array(
	   		'active'	=> 'Master',
	   		'content'	=> 'Kurir/list_kurir',
	   		'title'		=> 'Kurir',
	   		'list'		=> $this->Kurir_model->getList()
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	public function add(){	
		
	   	$data = array(
	   		'active'		=> 'Master',
	   		'content'		=> 'Kurir/form_kurir',
		   	'title'			=> 'New Kurir',
	   		'page_action' 	=> base_url('Kurir/save'),
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	function save(){
		
		$this->form_validation->set_rules('nama_kurir','Nama Kurir', 'trim|required');

		$accept = $this->fungsi->accept_data(array_keys($_POST));

		if ($this->form_validation->run() == FALSE) { 
			
			$this->session->set_flashdata('error', validation_errors());
			redirect('Kurir');

		} else {
			
			$data = array(
						'nama_kurir'				=> $accept['nama_kurir'],
						'telp_kurir'				=> $accept['telp_kurir'],
						'alamat_kurir'			=> $accept['alamat_kurir'],
						'aktif'						=> $accept['aktif']);

			$query = $this->Kurir_model->save($accept['id_kurir'], $data);

			if($query)
				$this->session->set_flashdata('success', 'Data berhasil disimpan');
			else
				$this->session->set_flashdata('error', 'Data gagal disimpan');
			
			redirect('Kurir');
		}	
	}

	public function delete($id){
		if (!$id) redirect('Kurir');	
		$query = $this->Kurir_model->delete($id);
		if($query)
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
		else
			$this->session->set_flashdata('error', 'Data gagal dihapus');
		
		redirect('Kurir');
	}

	function edit($id = 0){	
		if(!$this->Kurir_model->checkID($id)){
			$this->session->set_flashdata('error', 'Data tidak ditemukan');
			redirect('Kurir');
		};

	   	$data = array(
	   		'active'		=> 'Master',
	   		'content'		=> 'Kurir/form_kurir',
	   		'title'			=> 'Edit Kurir',
	   		'get_data'		=> $this->Kurir_model->edit($id),
	   		'page_action' 	=> base_url('Kurir/save')
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}
}