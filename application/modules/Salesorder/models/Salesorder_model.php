<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Salesorder_model extends CI_Model 
{    
	var $table = 'salesorder_header';
	var $primary = 'id_salesorder';

    public function __construct(){
        parent::__construct(); 
    }

    public function getList(){
        $query = $this->db->query("SELECT a.*,
                                    CASE WHEN a.status = 'D' THEN 'Draft' WHEN a.status = 'O' THEN 'Open' ELSE 'Close' END AS 'statussalesorder',
                                    b.nama_customer
                                    FROM salesorder_header a
                                    LEFT JOIN customer b ON a.id_customer = b.id_customer
                                    order by a.id_salesorder desc");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    public function getListReport($parameter='', $isExcel = FALSE){

        $where  = '';
        $where .= ($parameter['start_date'] != '' && $parameter['end_date'] != '') ? " AND a.tanggal_salesorder between '".date('Y-m-d',strtotime($parameter['start_date']))."' AND '".date('Y-m-d',strtotime($parameter['end_date']))."'" : "";
        $where .= ($parameter['id_customer'] != '') ? " AND a.id_customer = '".$parameter['id_customer']."'" : "";
        $where = (trim(substr($where,0,4)) == "AND") ? "WHERE ".substr($where,4,strlen($where)) : $where;

        if($isExcel){
            $query = $this->db->query("SELECT a.no_salesorder, b.nama_customer, a.tanggal_salesorder, a.subtotal, a.tax_rate, a.tax, a.total, a.termofpayment, a.note,
                                        CASE WHEN a.status = 'D' THEN 'Draft' WHEN a.status = 'O' THEN 'Open' ELSE 'Close' END AS 'statussalesorder'
                                        FROM salesorder_header a
                                        LEFT JOIN customer b ON a.id_customer = b.id_customer
                                        " .$where. "
                                        order by a.id_salesorder desc");
            if($query->num_rows()>0){
                return $query;
            } else {
                $query->free_result();
                return $query;
            }
        }else{
            $query = $this->db->query("SELECT a.*,
                                        CASE WHEN a.status = 'D' THEN 'Draft' WHEN a.status = 'O' THEN 'Open' ELSE 'Close' END AS 'statussalesorder',
                                        b.nama_customer
                                        FROM salesorder_header a
                                        LEFT JOIN customer b ON a.id_customer = b.id_customer
                                        " .$where. "
                                        order by a.id_salesorder desc");
            if($query->num_rows()>0){
                return $query;
            } else {
                $query->free_result();
                return $query;
            }
        }
    }

    function addData($data_header = '', $data_detail = ''){
    
        $this->db->trans_start();
        
        // Insert Into salesorder_header Start
        $this->db->set('created_user', $this->session->userdata('user_id'));
        $this->db->set('created_date', 'now()', FALSE);
        $this->db->set($data_header);
        
        $query = $this->db->insert('salesorder_header');
        if(!$query) return false;
        $ID = $this->db->insert_id();
        // Insert Into salesorder_header End
        
        // Insert Into salesorder_detail Start
        for($i = 0; $i < count($data_detail); $i++){
            $this->db->set('id_salesorder', $ID, FALSE);
            $this->db->set('created_user', $this->session->userdata('user_id'));
            $this->db->set('created_date', 'now()', FALSE);
            $this->db->set($data_detail[$i]);
            
            $query = $this->db->insert('salesorder_detail');
            if(!$query) return false;
        }
        // Insert Into salesorder_detail End
        
        $this->db->trans_complete();
        
        return true;
    }

    function updateData($data_header = '', $data_detail = '', $idsalesorder = 0){

        $this->db->trans_start();

        // Update Into salesorder_header Start
        $this->db->set('updated_user', $this->session->userdata('user_id'), FALSE);
        $this->db->set('updated_date', 'now()', FALSE);
        $this->db->set($data_header); 
        $this->db->where('id_salesorder', $idsalesorder); 
        $query = $this->db->update('salesorder_header');
        if(!$query) return false;
        // Update Into salesorder_header End

        // Update Into salesorder detail Start
        $itd    = '';
        foreach($data_detail as $row) {
            $itDetailID = $row['id_salesorder_detail'];
            unset($row['id_salesorder_detail']);
            
            if ($itDetailID > 0) { 
                
                $itd    .= ($itd == '') ? $itDetailID : ','.$itDetailID; 
                $this->db->set('id_salesorder', $idsalesorder, FALSE);
                $this->db->set('updated_user', $this->session->userdata('user_id'));
                $this->db->set('updated_date', 'now()', FALSE);
                $this->db->set($row);
                $this->db->where('id_salesorder_detail', $itDetailID); 
                $query = $this->db->update('salesorder_detail'); 

            }else { 
                
                $this->db->set('id_salesorder', $idsalesorder, FALSE);
                $this->db->set('created_user', $this->session->userdata('user_id'));
                $this->db->set('created_date', 'now()', FALSE);
                $this->db->set($row);
                $query = $this->db->insert('salesorder_detail'); 
                $ID = $this->db->insert_id();
                $itd    .= ($itd == '') ? $ID : ','.$ID; 

            } 
        }
        // Update Into salesorder_detail End

        if ($itd != '') {
            $this->db->query("DELETE FROM salesorder_detail where id_salesorder = ? and id_salesorder_detail not in (".$itd.")", $idsalesorder);
        }

                //insert stok start
        if ($data_header['status'] == 'O')
        {
            // die(var_dump($data_detail));
            foreach($data_detail as $row) {

                // stok
                $check_stock = $this->db->query("SELECT * FROM stock WHERE kode_produk = '".$row['kode_produk']."' ");
                if($check_stock->num_rows() > 0){

                    $total = $check_stock->row_object()->quantity - ($row['quantity'] * $row['order_quantity']);
                    if($total < 1) return false;
                    $this->db->set('quantity', $total);
                    $this->db->set('updated_user', $this->session->userdata('user_id'));
                    $this->db->set('updated_date', 'now()', FALSE);
                    $this->db->where('kode_produk', $row['kode_produk']);
                    $simpanstock = $this->db->update('stock');
                    if(!$simpanstock) return false;

                } else {

                    $this->db->set('kode_produk', $row['kode_produk']);
                    $this->db->set('quantity', ($row['quantity'] * $row['order_quantity']));
                    $this->db->set('created_user', $this->session->userdata('user_id'));
                    $this->db->set('created_date', 'now()', FALSE);
                    $simpanstocknew = $this->db->insert('stock');
                    if(!$simpanstocknew) return false;

                }

                // stok opname
                $this->db->set('no_salesorder', $data_header['no_salesorder']);
                $this->db->set('id_customer', $data_header['id_customer']);
                $this->db->set('tanggal_transaksi', $data_header['tanggal_salesorder']);
                $this->db->set('kode_produk', $row['kode_produk']);
                $this->db->set('quantity_keluar', ($row['quantity'] * $row['order_quantity']));
                $this->db->set('created_user', $this->session->userdata('user_id'));
                $this->db->set('created_date', 'now()', FALSE);
                $simpanstocknew = $this->db->insert('stock_opname');
                if(!$simpanstocknew) return false;

            }
        }
        //insert stok end

        $this->db->trans_complete(); 

        return true;
    }

    function checkID($id){
        $query = $this->db->get_where($this->table, array($this->primary=>$id));
        if ($query->num_rows() > 0) 
            return true;
        else
            return false;
    }

    public function getPrintDataHeader($id){
        $sql = "SELECT so.*, c.*,
                CASE WHEN so.status = 'D' THEN 'Draft' WHEN so.status = 'O' THEN 'Open' WHEN so.status = 'C' THEN 'Close' ELSE '' END 
                AS 'statussalesorder' 
                FROM salesorder_header so
                INNER JOIN customer c ON c.id_customer=so.id_customer
                WHERE so.id_salesorder = '".$id."'";

        $query = $this->db->query($sql);
        
        if($query->num_rows()>0){
            return $query;
        }else{
            $query->free_result();
            return $query;
        }
    }    

    public function edit($id){
        $query = $this->db->query("SELECT *, CASE WHEN status = 'D' THEN 'Draft' WHEN status = 'O' THEN 'Open' ELSE '' END AS 'statussalesorder' FROM salesorder_header WHERE id_salesorder = '".$id."'");
        return $query;
    }

    public function editdetail($id){
        $query = $this->db->query("SELECT *
                                    FROM salesorder_detail
                                    WHERE id_salesorder = '".$id."'
                                    order by line_num ");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    function search_item($id=''){

    	$ret = '';  
    	$sql = "
    			SELECT a.kode_produk, a.nama_produk, b.harga, s.quantity
    			FROM produk a
                left join harga b on a.kode_produk = b.kode_produk
                INNER JOIN stock s on a.kode_produk=s.kode_produk
    			WHERE (a.kode_produk like '%".$id."%' or a.nama_produk like '%".$id."%') and a.aktif = 'Y'
                AND s.quantity > 0
    			";

    	$q = $this->db->query($sql);

    	if ($q->num_rows() > 0) {
    		foreach($q->result() as $row) {
                $ret[] = array("kode_produk" 	=> $row->kode_produk, 
                               "nama_produk" 	=> $row->nama_produk,
                               "harga"          => $row->harga,
                               "quantity"       => $row->quantity
                           );
            }
    	}

    	return $ret;

    }

    function getCustomer(){
        $query = $this->db->query("SELECT id_customer, nama_customer from customer where aktif = 'Y' order by nama_customer asc");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }
}   
