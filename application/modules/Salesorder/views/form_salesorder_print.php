<div id="page">
		<style>
			@page{ margin: 0px; }
			html{ margin: 10px 10px 10px 10px;}
			body{
				margin: 0px;
				padding: 0px;
			}

			.report{
				padding: 10px 5px;
				background: #fff;
				margin: 15px;
				border: 1px solid #000;
				font-size: 14px;
				font-family: sans-serif;
			}

			.report-header{
				font-size: 20px;
				font-weight: bold;
				text-transform: uppercase;
				line-height: 28px;
			}
            
			.tblBarang td{
				border: 1px solid #000;
			}
            .report .row-header{
				font-weight: bold;
				text-align: center;
			}

			.report-title{
				text-align: center;
				font-weight: bold;
				font-size: 20px;
				margin-top: 30px;
				margin-bottom: 10px;
				text-transform: uppercase;
			}

			.report-meta{
				text-align: center;
				font-weight: bold;
				font-size: 12px;
				margin-bottom: 20px;
			}

			.report table{
				font-size: 12px;
				border-collapse: collapse;
				width: 100%;
				margin: 5px 0;
			}

			.report .item-model td{
				border: 1px solid #000;
				padding: 5px 10px;
				text-align: center;
			}
			
			.report .row-penyerahan-barang{
				width: 200px;
			}
			
			.report .approval{
				border-top: 1px solid #000;
				margin-top: 30px;
				page-break-inside: auto;
			}
			
			.report .approval td{
				text-align: center;
				border: 0px;
				vertical-align: bottom;
				font-weight: bold;
				width: 25%;
				padding-top: 20px;
			}
		</style>
		<?php foreach($get_data->result() as $so){ ?>
		<?php 
			$tgl = date("d", strtotime($so->tanggal_salesorder));
			$bln = date("m", strtotime($so->tanggal_salesorder));
			$thn = date("Y", strtotime($so->tanggal_salesorder));
			switch($bln){
				case 1 : $bln='Januari'; break;
				case 2 : $bln='Februari'; break;
				case 3 : $bln='Maret'; break;
				case 4 : $bln='April'; break;
				case 5 : $bln='Mei'; break;
				case 6 : $bln="Juni"; break;
				case 7 : $bln='Juli'; break;
				case 8 : $bln='Agustus'; break;
				case 9 : $bln='September'; break;
				case 10 : $bln='Oktober'; break;    
				case 11 : $bln='November'; break;
				case 12 : $bln='Desember'; break;
				default: $bln='UnKnown'; break;
			}
			
			$i = 1;
			$model[] = ''; 
			if($so->status == 'D'){
		?>
		<style type="text/css">
			.report{
				background: url('<?php echo site_url()?>/assets/admin/img/draft.png') center center no-repeat !important;
			}
		</style>
		<?php } ?>
		<div class="report">
			<div class="report-header">
				PT. Lumas Jaya Industri
			</div>		
			<div class="report-title">
				Sales Order (SO)
			</div>
			<div class="report-meta">
				PO No: <?php echo (isset($so->no_salesorder)) ? $so->no_salesorder : '';?>
				&nbsp;&nbsp;&nbsp;
				Tgl. <?php echo $tgl . ' ' . $bln . ' ' . $thn; ?>
			</div>
			
			Kepada Yth. :  <br />
            <table>
                <tr>
                    <td width="70%">
                        <table>
            				<tr>
            					<td width="120">Nama Customer</td>
            					<td>: <?php echo $so->nama_customer; ?></td>
            				</tr>
            				<tr>
            					<td>Alamat Pembeli</td>
								<td>: <?php echo (isset($so->alamat_customer)) ? $so->alamat_customer : ''; ?></td>
            				</tr>
            				<tr>
            					<td>Term Of Payment</td>
								<td>: <?php echo (isset($so->termofpayment)) ? $so->termofpayment : ''; ?></td>
            				</tr>
            			</table>
                    </td>
                </tr>
            </table>  
			
			<div style="min-height:30px;">
				<table class="tblBarang">
					<thead>
                          <tr>
                            <th class="row-header">No.</th>
                            <th class="row-header">Kode Produk</th>
                            <th class="row-header">Nama Produk</th>
                            <th class="row-header">Quantity / Pack</th>
                            <th class="row-header">Order</th>
                            <th class="row-header">Unit Price</th>
                            <th class="row-header">Total</th>
                          </tr>
					</thead>
					<tbody>
				<?php
					$i = 1;
					$totalrow  = $soD->num_rows();
					$totalPage = ceil($totalrow/40);
					$page = 1;
					foreach($soD->result() as $row){ 
						
						?> 
						<tr>
                            <td valign="top" align="center"><?php echo $i;?></td> 
                            <td valign="top" align="center"><?php echo $row->kode_produk; ?></td> 
                            <td valign="top" align="center"><?php echo $row->nama_produk; ?></td> 
                            <td valign="top" align="center"><?php echo $row->quantity; ?></td>
                            <td valign="top" align="center"><?php echo $row->order_quantity ?></td> 
                            <td valign="top" align="center"><?php echo number_format($row->harga, 0, ',' , '.'); ?></td> 
                            <td valign="top" align="center"><?php echo number_format($row->line_total, 0, ',' , '.'); ?></td>
						</tr> 
						<?php  
							$i++;
						}
						?> 
					</tbody>
				</table>
			</div>
			<table style="page-break-inside: auto;">
				<tr>
					<td>
						<table>
							<tr>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Subtotal</td>
								<td>: <?php echo number_format($get_data->row()->subtotal, 0, ',' , '.')?></td>
							</tr>
							<tr>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tax Rate (%)</td>
								<td>: <?php echo number_format($get_data->row()->tax_rate, 0, ',' , '.')?></td>
							</tr>
							<tr>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tax</td>
								<td>: <?php echo number_format($get_data->row()->tax, 0, ',' , '.')?></td>
							</tr>
							<tr>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total</td>
								<td>: <?php echo number_format($get_data->row()->total, 0, ',' , '.')?></td>
							</tr>							
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Keterangan Lain</strong></td>
							</tr>
							<tr>
								<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $so->note; ?></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	<?php } ?>
</div>