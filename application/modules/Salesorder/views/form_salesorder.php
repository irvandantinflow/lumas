<div class="modal fade" id="myModalItemLookup" tabindex="-1" role="basic" aria-hidden="true"> 
    <?php echo $this->load->view('itemLookup');?> 
</div>

<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left"></div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group"></div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">

      <div class="col-md-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?php echo $title; ?> </h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>

          <form class="form-horizontal form-label-left" method="POST" action="<?php echo $page_action;?>" >
          <!-- HEADER SIDE -->
          <div class="x_content">
            <br />

              <!-- LEFT COLOUM -->
              <div class="col-md-6">

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">No. Sales Order</label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" class="form-control" placeholder="No. Sales Order" name="no_salesorder" value="<?php echo isset($get_data) ? $get_data->row()->no_salesorder : ''; ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Customer</label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <select class="form-control select2_single" name="id_customer" required="">
                      <option value="">-- Select Customer --</option>
                      <?php foreach ($customer->result() as $baris) {
                        if (isset($get_data) && $baris->id_customer == $get_data->row()->id_customer) { ?>
                        <option value="<?php echo $baris->id_customer; ?>" selected><?php echo strtoupper($baris->nama_customer); ?></option>
                         <?php } else { ?>
                        <option value="<?php echo $baris->id_customer; ?>"><?php echo strtoupper($baris->nama_customer); ?></option>
                      <?php } }?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <?php if (isset($get_data) && $get_data->row()->status == 'D'){?>
                      <select class="form-control select2_single" style="width: 100%;" name="status">
                        <option value="D" <?php if(isset($get_data) && $get_data->row()->status == "D") echo 'selected'; ?>>Draft</option>
                        <option value="O" <?php if(isset($get_data) && $get_data->row()->status == "O") echo 'selected'; ?>>Open</option>
                      </select>
                    <?php } else { ?>
                      <input type="hidden" name="status" value="<?php echo isset($get_data) ? $get_data->row()->status : ''; ?>">
                      <input type="text" class="form-control" name="statussalesorder" readonly="" value="<?php echo isset($get_data) ? $get_data->row()->statussalesorder : 'Draft'; ?>">
                    <?php } ?>
                  </div>
                </div>

              </div>
              <!-- LEFT COLOUM -->

              <!-- RIGHT COLOUM -->
              <div class="col-md-6">

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal</label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" class="form-control has-feedback-left datepicker" id="tanggal_salesorder" name="tanggal_salesorder" placeholder="Tanggal Sales order" aria-describedby="inputSuccess2Status3" value="<?php echo isset($get_data) ? date('d-m-Y',strtotime($get_data->row()->tanggal_salesorder)) : date('d-m-Y'); ?>" data-date-format="dd-mm-yyyy">
                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Term Of Payment</label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <?php if (!isset($get_data) || $get_data->row()->status == 'D'){?>
                      <select class="form-control select2_single" style="width: 100%;" name="termofpayment">
                        <option value="Cash" <?php if(isset($get_data) && $get_data->row()->termofpayment == "Cash") echo 'selected'; ?>>Cash</option>
                        <option value="Transfer" <?php if(isset($get_data) && $get_data->row()->termofpayment == "Transfer") echo 'selected'; ?>>Transfer</option>
                      </select>
                    <?php } else { ?>
                      <input type="text" class="form-control" name="termofpayment" readonly="" value="<?php echo isset($get_data) ? $get_data->row()->termofpayment : ''; ?>">
                    <?php } ?>
                  </div>
                </div>

              </div>
              <!-- RIGHT COLOUM -->

          </div>
          <!-- HEADER SIDE -->

          <!-- DETAIL SIDE -->
          <div class="x_content">
            <br />

              <!-- LEFT COLOUM -->
              <div class="col-md-12">

                <?php if(!isset($get_data) || isset($get_data) && $get_data->row()->statussalesorder == "Draft"){?>
                  <div class="row">          
                    <div class="col-md-6">
                      <button type="button" name="deletelines" id="deletelines" class="btn btn-sm"><i class="fa fa-times"></i> Hapus Baris Yang Dipilih</button>
                    </div> 
                    <div class="col-md-6" style="text-align:right;">
                      <button type="button" name="initTable" id="initTable" class="btn btn-sm"><i class="fa fa-trash-o"></i> Hapus Seluruh Baris</button>
                      <button type="button" name="morelines" id="morelines" class="btn btn-sm"><i class="fa fa-plus"></i> Tambah Item</button>
                      <a name="ItemLookup" id="ItemLookup" class="btn btn-sm hide" href="#myModalItemLookup" data-toggle="modal" data-backdrop="static"><i class="fa fa-plus"> Tambah Item</i></a>
                    </div> 
                  </div>
                <?php } ?>

                <div class="row">
                  <!-- DETAIL-->
                  <div class="col-md-12"> 
                    <div class="table-scrollablexy">
                      <table class="table table-bordered table-advance table-hover" id="datatables">                        
                        <thead>
                          <tr>
                            <?php if(!isset($get_data) || isset($get_data) && $get_data->row()->statussalesorder == "Draft"){?>
                              <th style="width:8px"></th>
                            <?php } ?>
                            <th><span class="hidden-phone">No.</span></th>
                            <th><span class="hidden-phone">Item #</span></th>
                            <th><span class="hidden-phone">Description</span></th>
                            <th><span class="hidden-phone">Quantity / Pack</span></th>
                            <th><span class="hidden-phone">Order</span></th>
                            <th><span class="hidden-phone">Unit Price</span></th>
                            <th><span class="hidden-phone">Total</span></th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                            $i = 1;
                            if (!empty($get_data_detail)){
                              foreach($get_data_detail->result() as $row){ 
                          ?>
                          <tr class="tblRowSO">
                            <?php if(!isset($get_data) || isset($get_data) && $get_data->row()->statussalesorder == "Draft"){?>
                              <td><input type="checkbox" class="checkboxes lineitem" value="" name="lineitem" /></td>
                            <?php } ?>  
                            <td><?php echo $i;?></td> 
                            <td><?php echo $row->kode_produk; ?></td> 
                            <td><?php echo $row->nama_produk; ?></td> 
                            <td>
                              <input type="text" name="order[<?php echo $i-1;?>][quantity]" class="txtbdu numeric" id="quantity" size="5" value="<?php echo (isset($row->quantity)) ? $row->quantity : ''; ?>" />
                              <input type="hidden" id="kode_produk" name="order[<?php echo $i-1;?>][kode_produk]" value="<?php echo (isset($row->kode_produk)) ? $row->kode_produk : ''; ?>" />
                              <input type="hidden" id="nama_produk" name="order[<?php echo $i-1;?>][nama_produk]" value="<?php echo (isset($row->nama_produk)) ? $row->nama_produk : ''; ?>" />
                              <input type="hidden" id="id_salesorder" name="order[<?php echo $i-1;?>][id_salesorder]" value="<?php echo (isset($row->id_salesorder)) ? $row->id_salesorder : ''; ?>" />
                              <input type="hidden" id="id_salesorder_detail" name="order[<?php echo $i-1;?>][id_salesorder_detail]" value="<?php echo (isset($row->id_salesorder_detail)) ? $row->id_salesorder_detail : ''; ?>" />
                              <input type="hidden" id="harga" name="order[<?php echo $i-1;?>][harga]" value="<?php echo (isset($row->harga)) ? $row->harga : '0'; ?>" />
                              <input type="hidden" id="line_total" name="order[<?php echo $i-1;?>][line_total]" value="<?php echo (isset($row->line_total)) ? $row->line_total : ''; ?>" />
                            </td>
                            <td>
                              <input type="text" name="order[<?php echo $i-1;?>][order_quantity]" class="txtbdu numeric" id="order_quantity" size="5" value="<?php echo (isset($row->order_quantity)) ? $row->order_quantity : ''; ?>" />
                            </td> 
                            <td><?php echo number_format($row->harga, 0, ',' , '.'); ?></td> 
                            <td><?php echo number_format($row->line_total, 0, ',' , '.'); ?></td> 
                          </tr>  
                          <?php
                            $i++;
                              }
                            } else {
                          ?>
                          <tr class="tblRowSO">
                            <?php if(!isset($get_data) || isset($get_data) && $get_data->row()->statussalesorder == "Draft"){?>
                              <td><input type="checkbox" class="checkboxes lineitem" value="" name="lineitem" /></td>
                            <?php } ?>
                            <td><?php echo $i;?></td> 
                            <td></td> 
                            <td></td> 
                            <td>
                              <input type="text" name="order[<?php echo $i-1;?>][quantity]" class="txtbdu numeric" id="quantity" size="5" value="" />
                              <input type="hidden" id="kode_produk" name="order[<?php echo $i-1;?>][kode_produk]" value="" />
                              <input type="hidden" id="nama_produk" name="order[<?php echo $i-1;?>][nama_produk]" value="" />
                              <input type="hidden" id="id_salesorder" name="order[<?php echo $i-1;?>][id_salesorder]" value="" />
                              <input type="hidden" id="id_salesorder_detail" name="order[<?php echo $i-1;?>][id_salesorder_detail]" value="" />
                              <input type="hidden" id="harga" name="order[<?php echo $i-1;?>][harga]" value="" />
                              <input type="hidden" id="line_total" name="order[<?php echo $i-1;?>][line_total]" value="" />
                            </td>
                            <td>
                              <input type="text" name="order[<?php echo $i-1;?>][order_quantity]" class="txtbdu numeric" id="order_quantity" size="5" value="" />
                            </td> 
                            <td></td> 
                            <td></td> 
                          </tr>
                          <?php 
                            }
                          ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

                <div class="ln_solid"></div>
                
              </div>
              <!-- LEFT COLOUM -->

          </div>
          <!-- DETAIL SIDE -->

          <!-- HEADER SIDE -->
          <div class="x_content">
            <br />

              <div class="row">
                <!-- LEFT COLOUM -->
                <div class="col-md-6">

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Notes</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <textarea id="note" name="note" rows="3" class="form-control input-medium"><?php echo isset($get_data) ? $get_data->row()->note : ''; ?></textarea>
                    </div>
                  </div>
                  

                </div>
                <!-- LEFT COLOUM -->

                <!-- RIGHT COLOUM -->
                <div class="col-md-6">

                  <div class="form-group">
                    <label class="control-label col-md-6 col-sm-6 col-xs-12">Subtotal</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="text" class="form-control" placeholder="Subtotal" name="subtotal" id="subtotal" readonly="" value="<?php echo isset($get_data) ? number_format($get_data->row()->subtotal, 0, ',' , '.') : number_format(0, 0, ',' , '.'); ?>" style="text-align: right;">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-6 col-sm-6 col-xs-12">Tax Rate (%)</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="text" class="form-control" placeholder="Tax Rate" name="tax_rate" id="tax_rate" readonly="" value="<?php echo isset($get_data) ? number_format($get_data->row()->tax_rate, 0, ',' , '.') : number_format(10, 0, ',' , '.'); ?>" style="text-align: right;">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-6 col-sm-6 col-xs-12">Tax</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="text" class="form-control" placeholder="Tax" name="tax" id="tax" readonly="" value="<?php echo isset($get_data) ? number_format($get_data->row()->tax, 0, ',' , '.') : number_format(0, 0, ',' , '.'); ?>" style="text-align: right;">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-6 col-sm-6 col-xs-12">Total</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="text" class="form-control " placeholder="Total" name="total" id="total" readonly="" value="<?php echo isset($get_data) ? number_format($get_data->row()->total, 0, ',' , '.') : number_format(0, 0, ',' , '.'); ?>" style="text-align: right;">
                    </div>
                  </div>

                </div>
                <!-- RIGHT COLOUM -->
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <input type="hidden" name="id_salesorder" value="<?php echo isset($get_data) ? $get_data->row()->id_salesorder : ''; ?>"/>
                      <a href="<?php echo base_url('Salesorder'); ?>" class="btn btn-primary" name="Back">Back</a>
                      <?php if(!isset($get_data) || isset($get_data) && $get_data->row()->statussalesorder == "Draft"){?>
                        <button type="submit" class="btn btn-success" name="submit">Submit</button>
                      <?php } ?>
                      <?php if(isset($get_data)){?>
                      <a class="btn btn-warning" href="<?php echo base_url();?>Salesorder/printdata/<?php echo $row->id_salesorder;?>" target="_blank">
                        <i class="fa fa-print icon-white"></i>
                        Print Form
                      </a>
                      <?php } ?>                      
                    </div>
                  </div>
                </div>
              </div>

          </div>
          <!-- HEADER SIDE -->
          </form>

        </div>
      </div>
    </div>
  </div>
</div>

<!-- ADD AFTER jquery.min.js -->
<!-- <script src="<?php echo base_url();?>assets/admin/vendors/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/permintaan.js"></script> -->