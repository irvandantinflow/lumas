<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Salesorder extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Salesorder_model');
		if(!is_logged_in()){redirect('Inadminpage');}	
		// if(!is_access()){
		// 	$this->session->set_flashdata('error', 'Anda tidak berhak mengakses halaman ini');
		// 	redirect('Home');
		// }
	}

	public function index(){	
		
	   	$data = array(
	   		'active'	=> 'Sales',
	   		'content'	=> 'Salesorder/list_salesorder',
	   		'title'		=> 'Sales Order',
	   		'list'		=> $this->Salesorder_model->getList()
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	public function report(){
		$list=''; 
		if (isset($_POST['submit'])){
			$filterParam = $this->fungsi->accept_data(array_keys($_POST));  
			if ($_POST['submit'] == 'findData'){ 
				$list = $this->Salesorder_model->getListReport($filterParam);
			} elseif($_POST['submit'] == 'exportToExcel'){	 
				$this->excel_this($filterParam);
					return true;
			} 
		} else {
			$filterParam = '';
		}	
		
	   	$data = array(
	   		'active'	=> 'Report',
	   		'content'	=> 'Salesorder/list_salesorder_report',
	   		'title'		=> 'Report Sales Order',
	   		'list'		=> $list,
	   		'customer'		=> 	$this->Salesorder_model->getCustomer(),
	   		'param'     => $filterParam
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	function excel_this($data = ''){
		 
		if($data == ''){
			$this->session->set_flashdata('error', 'Tidak ada parameter');
			redirect('Salesorder/list_salesorder_report');
		}
		$this->load->library('to_excel');
		
		if($data['id_customer'] != '')	$namacus = $this->db->query("SELECT * FROM customer WHERE id_customer = '" . $data['id_customer'] . "'")->row()->nama_id_customer;
		else							$namacus = '- Semua Customer -';
		
		
		$title	 = '<div style="font-size: 24px"><strong>PT LUMAS JAYA INDUSTRI</strong></div>';
		$title	.= '<div style="font-size: 18px"><strong>Laporan Sales Order (SO)</strong></div><br />';
		$title	.= '<strong>Tanggal Cetak Report: ' . date('d-m-Y') . '</strong><br />';
		$title	.= '<strong>Periode Report: ' . $data['start_date'] . ' s/d ' . $data['end_date'] . '</strong><br />';
		$title	.= '<strong>Nama Customer : ' . $namacus . '</strong><br />'; 
		$title	.= '<br />';
		
		$dataRpt = $this->Salesorder_model->getListReport($data, TRUE)->result_array();
		to_excel($dataRpt,
			'SO_Report-' . date('dmY'),
			$title
		);
	}	

	public function add(){	
		
	   	$data = array(
	   		'active'		=> 'Sales',
	   		'content'		=> 'Salesorder/form_salesorder',
		   	'title'			=> 'New Sales Order',
		   	'customer'		=> 	$this->Salesorder_model->getCustomer(),
		   	'page_action' 	=> base_url('Salesorder/add_process'),
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	function add_process(){
		
		$data 	= $this->fungsi->accept_data(array_keys($_POST));

		//detail data
		$order = $data['order'];
		$i = 0;
		foreach($order as $row)
		{
			$data_detail[] = array(
				'line_num'			=> $i,
				'kode_produk'		=> $row['kode_produk'],
				'nama_produk'		=> $row['nama_produk'],
				'quantity'			=> $row['quantity'],
				'order_quantity'	=> $row['order_quantity'],
				'harga'				=> $row['harga'],
				'line_total'		=> $row['line_total']	
			);
			$i++; 
		}

		//header data
		$data_header = array(
			'no_salesorder'			=> $data['no_salesorder'],
			'id_customer'			=> $data['id_customer'],
			'status'				=> 'D',
			'tanggal_salesorder'		=> isset($data['tanggal_salesorder']) ? date('Y-m-d', strtotime($data['tanggal_salesorder'])) : '',
			'note'					=> $data['note'],
			'subtotal'				=> number_unformat($data['subtotal']),
			'tax_rate'				=> number_unformat($data['tax_rate']),
			'tax'					=> number_unformat($data['tax']),
			'termofpayment'					=> $data['termofpayment'],
			'total'					=> number_unformat($data['total'])
		);

		$query = $this->Salesorder_model->addData($data_header, $data_detail);
		
		if($query)
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
		else
			$this->session->set_flashdata('error', 'Data gagal disimpan');
		
		redirect('Salesorder');

	}

	function edit($id = 0){	
		if(!$this->Salesorder_model->checkID($id)){
			$this->session->set_flashdata('error', 'Data tidak ditemukan');
			redirect('Salesorder');
		};

	   	$data = array(
	   		'active'		=> 'Sales',
	   		'content'		=> 'Salesorder/form_salesorder',
	   		'title'			=> 'Edit Sales order',
	   		'page_action' 	=> base_url('Salesorder/update_process'),
	   		'customer'		=> 	$this->Salesorder_model->getCustomer(),
	   		'get_data'		=> $this->Salesorder_model->edit($id),
	   		'get_data_detail'	=> $this->Salesorder_model->editdetail($id)
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	function update_process(){
		
		$data 			= $this->fungsi->accept_data(array_keys($_POST));
		$idsalesorder		= $data['id_salesorder'];

		//detail data
		$order = $data['order'];
		$i = 0;
		foreach($order as $row)
		{
			$data_detail[] = array(
				'line_num'					=> $i,
				'kode_produk'				=> $row['kode_produk'],
				'nama_produk'				=> $row['nama_produk'],
				'quantity'					=> $row['quantity'],
				'order_quantity'			=> $row['order_quantity'],
				'harga'						=> $row['harga'],
				'line_total'				=> $row['line_total'],
				'id_salesorder_detail'		=> isset($row['id_salesorder_detail']) ? $row['id_salesorder_detail'] : 0,	
			);
			$i++; 
		}

		//header data
		$data_header = array(
			'no_salesorder'			=> $data['no_salesorder'],
			'id_customer'			=> $data['id_customer'],
			'status'				=> $data['status'],
			'tanggal_salesorder'		=> isset($data['tanggal_salesorder']) ? date('Y-m-d', strtotime($data['tanggal_salesorder'])) : '',
			'note'					=> $data['note'],
			'subtotal'				=> number_unformat($data['subtotal']),
			'tax_rate'				=> number_unformat($data['tax_rate']),
			'tax'					=> number_unformat($data['tax']),
			'termofpayment'					=> $data['termofpayment'],
			'total'					=> number_unformat($data['total'])
		);

		$query = $this->Salesorder_model->updateData($data_header, $data_detail, $idsalesorder);
		
		if($query)
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
		else
			$this->session->set_flashdata('error', 'Data gagal disimpan');
		
		redirect('Salesorder');

	}

	function searchItem($itemCode=''){ 
		$itemCode = urldecode($itemCode);
		$retData  = $this->Salesorder_model->search_item($itemCode);
		
		echo json_encode($retData);
	}

	function printdata($id = 0){	
		if(!$this->Salesorder_model->checkID($id)){
			$this->session->set_flashdata('error', 'Data tidak ditemukan');
			redirect('Purchase');
		};

		$get_data = $this->Salesorder_model->getPrintDataHeader($id);
	   	$data = array(
	   		'active'		=> 'Sales',
	   		'print_page'	=> 'Print Page',
	   		'content'		=> 'Salesorder/form_salesorder_print',
	   		'title'			=> 'Form Sales Order',
	   		'page_action' 	=> base_url('Salesorder/update_process'),
	   		'get_data'		=> $get_data,
	   		'soD'	=> $this->Salesorder_model->editdetail($id)
		);

		// $this->load->view($data['content'], $data);
	   	$html = $this->load->view($data['content'], $data, TRUE);

		$filename = "SO_" . $get_data->row()->no_salesorder;
		$filename .= "_" . date('dmY');
		$filename .= ".pdf";
		
		$this->load->library('dompdf_gen');
		
		// Convert to PDF
		$this->dompdf->load_html($html);
		$this->dompdf->set_paper("A4"); 
		$this->dompdf->render();
		$this->dompdf->stream($filename);	   	
	}	
}