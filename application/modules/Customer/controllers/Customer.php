<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Customer_model');
		if(!is_logged_in()){redirect('Inadminpage');}	
		// if(!is_access()){
		// 	$this->session->set_flashdata('error', 'Anda tidak berhak mengakses halaman ini');
		// 	redirect('Home');
		// }
	}

	public function index(){	
		
	   	$data = array(
	   		'active'	=> 'Sales',
	   		'content'	=> 'Customer/list_customer',
	   		'title'		=> 'Customer',
	   		'list'		=> $this->Customer_model->getList()
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	public function add(){	
		
	   	$data = array(
	   		'active'		=> 'Sales',
	   		'content'		=> 'Customer/form_customer',
		   	'title'			=> 'New Customer',
	   		'page_action' 	=> base_url('Customer/save'),
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	function save(){
		
		$this->form_validation->set_rules('nama_customer','Nama Customer', 'trim|required');

		$accept = $this->fungsi->accept_data(array_keys($_POST));

		if ($this->form_validation->run() == FALSE) { 
			
			$this->session->set_flashdata('error', validation_errors());
			redirect('Customer');

		} else {
			
			$data = array(
						'nama_customer'				=> $accept['nama_customer'],
						'telp_customer'				=> $accept['telp_customer'],
						'alamat_customer'			=> $accept['alamat_customer'],
						'aktif'						=> $accept['aktif']);

			$query = $this->Customer_model->save($accept['id_customer'], $data);

			if($query)
				$this->session->set_flashdata('success', 'Data berhasil disimpan');
			else
				$this->session->set_flashdata('error', 'Data gagal disimpan');
			
			redirect('Customer');
		}	
	}

	public function delete($id){
		if (!$id) redirect('Customer');	
		$query = $this->Customer_model->delete($id);
		if($query)
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
		else
			$this->session->set_flashdata('error', 'Data gagal dihapus');
		
		redirect('Customer');
	}

	function edit($id = 0){	
		if(!$this->Customer_model->checkID($id)){
			$this->session->set_flashdata('error', 'Data tidak ditemukan');
			redirect('Customer');
		};

	   	$data = array(
	   		'active'		=> 'Sales',
	   		'content'		=> 'Customer/form_customer',
	   		'title'			=> 'Edit Customer',
	   		'get_data'		=> $this->Customer_model->edit($id),
	   		'page_action' 	=> base_url('Customer/save')
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}
}