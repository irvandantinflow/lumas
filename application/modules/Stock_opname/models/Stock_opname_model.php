<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stock_opname_model extends CI_Model 
{

    public function __construct(){
        parent::__construct(); 
    }

    public function getList(){
        $query = $this->db->query("SELECT T0.*, 
        							T1.nama_produk
        							FROM stock_opname T0
        							LEFT JOIN produk T1 ON T0.kode_produk = T1.kode_produk
        							order by T0.id_stock asc");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    public function getProduk(){
        $query = $this->db->query("SELECT DISTINCT kode_produk
                                    FROM stock_opname
                                    order by kode_produk asc");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    public function getKartuStock($id){
        $query = $this->db->query("SELECT T0.*, 
                                    T1.nama_produk,
                                    T2.nama_supplier,
                                    T3.nama_customer,
                                    CASE
                                        WHEN T0.id_supplier > 0 THEN T2.nama_supplier
                                        ELSE T3.nama_customer
                                    END AS 'name'
                                    FROM stock_opname T0
                                    LEFT JOIN produk T1 ON T0.kode_produk = T1.kode_produk
                                    LEFT JOIN supplier T2 ON T0.id_supplier = T2.id_supplier
                                    LEFT JOIN customer T3 ON T0.id_customer = T3.id_customer
                                    WHERE T0.kode_produk = '".$id."'
                                    order by T0.id_stock asc");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }
}
