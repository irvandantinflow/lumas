<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <?php echo $this->load->view('alert');?>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search"></div>
      </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?php echo $title; ?> </h2>
            <div class="clearfix"></div>
            <div class="" role="tabpanel" data-example-id="togglable-tabs">
              <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                <?php 
                $no = 1;
                if ($list_produk != '') :
                foreach($list_produk->result() as $row){ 
                ?>
                  <li role="presentation" class="<?php echo ($no == 1) ? 'active' : ''; ?>"><a href="#<?php echo $row->kode_produk; ?>" id="CODE<?php echo $row->kode_produk; ?>" role="tab" data-toggle="tab" aria-expanded="true"><?php echo $row->kode_produk; ?></a></li>
                <?php $no++; } endif;?>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br>
              <div id="myTabContent" class="tab-content">
                <?php 
                $no = 1;
                if ($list_produk != '') :
                foreach($list_produk->result() as $row){ 
                ?>
                <div role="tabpanel" class="tab-pane fade <?php echo ($no == 1) ? 'active in' : ''; ?>" id="<?php echo $row->kode_produk; ?>" aria-labelledby="pc-tab">
                  <table id="datatable-responsive" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th>No.</th>
                        <th>Tanggal</th>
                        <th>No. Purchase</th>
                        <th>No. Sales Order</th>
                        <th>No. Retur</th>
                        <th>Name</th>
                        <th>Kode Produk</th>
                        <th>Nama Produk</th>
                        <th>Quantity Masuk</th>
                        <th>Quantity Keluar</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $no = 1;
                      $list_kartustock = $this->Stock_opname_model->getKartuStock($row->kode_produk);
                      foreach($list_kartustock->result() as $row){ 
                      ?>
                      <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo date('d-m-Y',strtotime($row->tanggal_transaksi)); ?></td>
                        <td><?php echo $row->no_purchase; ?></td>
                        <td><?php echo $row->no_salesorder; ?></td>
                        <td><?php echo $row->no_retur; ?></td>
                        <td><?php echo $row->name; ?></td>
                        <td><?php echo $row->kode_produk; ?></td>
                        <td><?php echo $row->nama_produk; ?></td>
                        <td><?php echo ($row->quantity_masuk == '0') ? '' : $row->quantity_masuk; ?></td>
                        <td><?php echo ($row->quantity_keluar == '0') ? '' : $row->quantity_keluar; ?></td>
                      </tr>
                      <?php }?>
                    </tbody>
                  </table>
                </div>
                <?php $no++; } endif;?>                
              </div>
            </div>

            <div class="clearfix"></div>
          </div>
          
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->