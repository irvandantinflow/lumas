<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stock_opname extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Stock_opname_model');
		if(!is_logged_in()){redirect('Inadminpage');}	
		// if(!is_access()){
		// 	$this->session->set_flashdata('error', 'Anda tidak berhak mengakses halaman ini');
		// 	redirect('Home');
		// }
	}

	public function index(){	
		
	   	$data = array(
	   		'active'	=> 'Inventory',
	   		'content'	=> 'Stock_opname/list_stock_opname',
	   		'title'		=> 'Stock Opname',
	   		'list'		=> $this->Stock_opname_model->getList()
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	public function kartu_stock(){	
		
	   	$data = array(
	   		'active'	=> 'Inventory',
	   		'content'	=> 'Stock_opname/list_kartustock',
	   		'title'		=> 'Kartu Stock',
	   		'list_produk'		=> $this->Stock_opname_model->getProduk()
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}
}