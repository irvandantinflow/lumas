<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Produk_model');
		if(!is_logged_in()){redirect('Inadminpage');}	
		// if(!is_access()){
		// 	$this->session->set_flashdata('error', 'Anda tidak berhak mengakses halaman ini');
		// 	redirect('Home');
		// }
	}

	public function index(){	
		
	   	$data = array(
	   		'active'	=> 'Inventory',
	   		'content'	=> 'Produk/list_produk',
	   		'list'		=> $this->Produk_model->getList()
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	public function add(){	
		
	   	$data = array(
	   		'active'		=> 'Inventory',
	   		'content'		=> 'Produk/form_produk',
		   	'title'			=> 'New Produk',
	   		'page_action' 	=> base_url('Produk/save'),
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	function save(){
		
		$this->form_validation->set_rules('kode_produk','Kode Produk', 'trim|required');
		$this->form_validation->set_rules('nama_produk','Nama Produk', 'trim|required');

		$accept = $this->fungsi->accept_data(array_keys($_POST));

		if($this->Produk_model->checkID($accept['kode_produk'])){
            $this->session->set_flashdata('error', 'Kode produk sudah ada');
            redirect('Produk');
        };	

		if ($this->form_validation->run() == FALSE) { 
			
			$this->session->set_flashdata('error', validation_errors());
			redirect('Produk');

		} else {
			
			$data = array(
						'kode_produk'			=> $accept['kode_produk'],
						'nama_produk'			=> $accept['nama_produk'],
						'aktif'					=> $accept['aktif']);

			$query = $this->Produk_model->save('', $data);

			if($query)
				$this->session->set_flashdata('success', 'Data berhasil disimpan');
			else
				$this->session->set_flashdata('error', 'Data gagal disimpan');
			
			redirect('Produk');
		}	
	}

	function update(){
		
		$this->form_validation->set_rules('kode_produk','Kode Produk', 'trim|required');
		$this->form_validation->set_rules('nama_produk','Nama Produk', 'trim|required');

		$accept = $this->fungsi->accept_data(array_keys($_POST));

		if ($this->form_validation->run() == FALSE) { 
			
			$this->session->set_flashdata('error', validation_errors());
			redirect('Produk');

		} else {
			
			$data = array(
						'nama_produk'			=> $accept['nama_produk'],
						'aktif'					=> $accept['aktif']);

			$query = $this->Produk_model->save($accept['kode_produk'], $data);

			if($query)
				$this->session->set_flashdata('success', 'Data berhasil disimpan');
			else
				$this->session->set_flashdata('error', 'Data gagal disimpan');
			
			redirect('Produk');
		}	
	}

	public function delete($id){
		if (!$id) redirect('Produk');	
		$query = $this->Produk_model->delete($id);
		if($query)
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
		else
			$this->session->set_flashdata('error', 'Data gagal dihapus');
		
		redirect('Produk');
	}

	function edit($id = 0){	
		if(!$this->Produk_model->checkID($id)){
			$this->session->set_flashdata('error', 'Data tidak ditemukan');
			redirect('Produk');
		};

	   	$data = array(
	   		'active'		=> 'Inventory',
	   		'content'		=> 'Produk/form_produk',
	   		'title'			=> 'Edit Produk',
	   		'page_action' 	=> base_url('Produk/update'),
	   		'get_data'		=> $this->Produk_model->edit($id),
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}
}