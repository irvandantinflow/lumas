<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Retur extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Retur_model');
		if(!is_logged_in()){redirect('Inadminpage');}	
		// if(!is_access()){
		// 	$this->session->set_flashdata('error', 'Anda tidak berhak mengakses halaman ini');
		// 	redirect('Home');
		// }
	}

	public function index(){	
		
	   	$data = array(
	   		'active'	=> 'Purchasing',
	   		'content'	=> 'Retur/list_retur',
	   		'title'		=> 'Retur',
	   		'list'		=> $this->Retur_model->getList()
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	public function add($id=0){	
		if($id){
			if(!$this->Retur_model->checkPOByID($id)){
				$this->session->set_flashdata('error', 'No. Purchase Order Tidak ditemukan.');
    			redirect('Retur'); 
			}
			if(!$this->Retur_model->checkPOStatusByID($id)) {
                $this->session->set_flashdata('error', 'Status Purchase Order Sudah Close.');
    			redirect('Retur'); 
            } 
		}
	   	$data = array(
	   		'active'		=> 'Purchasing',
	   		'content'		=> 'Retur/form_retur',
		   	'title'			=> 'New Retur',
		   	'supplier'		=> 	$this->Retur_model->getSupplierbyPOID($id),
		   	'page_action' 	=> base_url('Retur/add_process'),
	   		'get_dataDOH'		=> $this->Retur_model->getPOH($id),
	   		'get_data_detail'	=> $this->Retur_model->getPOD($id)
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	function add_process(){
		
		$data 	= $this->fungsi->accept_data(array_keys($_POST));

		//detail data
		$order = $data['order'];
		$i = 0;
		foreach($order as $row)
		{
			$data_detail[] = array(
				'line_num'			=> $i,
				'kode_produk'		=> $row['kode_produk'],
				'nama_produk'		=> $row['nama_produk'],
				'quantity'			=> $row['quantity'],
				'order_quantity'	=> $row['order_quantity'],
				'harga'				=> $row['harga'],
				'line_total'		=> $row['line_total']	
			);
			$i++; 
		}

		//header data
		$data_header = array(
			'no_retur'				=> $data['no_retur'],
			'id_supplier'			=> $data['id_supplier'],
			'status'				=> 'D',
			'tanggal_retur'			=> isset($data['tanggal_retur']) ? date('Y-m-d', strtotime($data['tanggal_retur'])) : '',
			'note'					=> $data['note'],
			'subtotal'				=> number_unformat($data['subtotal']),
			'tax_rate'				=> number_unformat($data['tax_rate']),
			'tax'					=> number_unformat($data['tax']),
			'total'					=> number_unformat($data['total']),
			'id_purchase'			=> $data['id_purchase']
		);

		$query = $this->Retur_model->addData($data_header, $data_detail);
		
		if($query)
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
		else
			$this->session->set_flashdata('error', 'Data gagal disimpan');
		
		redirect('Retur');

	}

	function edit($id = 0){	
		if(!$this->Retur_model->checkID($id)){
			$this->session->set_flashdata('error', 'Data tidak ditemukan');
			redirect('Retur');
		};

	   	$data = array(
	   		'active'		=> 'Purchasing',
	   		'content'		=> 'Retur/form_retur',
	   		'title'			=> 'Edit Retur',
	   		'page_action' 	=> base_url('Retur/update_process'),
	   		'supplier'		=> 	$this->Retur_model->getSupplierbyRHID($id),
	   		'get_dataDOH'		=> $this->Retur_model->edit($id),
	   		'get_data_detail'	=> $this->Retur_model->editdetail($id)
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	function update_process(){
		
		$data 			= $this->fungsi->accept_data(array_keys($_POST));
		$idretur		= $data['id_retur'];

		//detail data
		$order = $data['order'];
		$i = 0;
		foreach($order as $row)
		{
			$data_detail[] = array(
				'line_num'					=> $i,
				'kode_produk'				=> $row['kode_produk'],
				'nama_produk'				=> $row['nama_produk'],
				'quantity'					=> $row['quantity'],
				'order_quantity'			=> $row['order_quantity'],
				'harga'						=> $row['harga'],
				'line_total'				=> $row['line_total'],
				'id_retur_detail'		=> isset($row['id_retur_detail']) ? $row['id_retur_detail'] : 0,	
			);
			$i++; 
		}

		//header data
		$data_header = array(
			'no_retur'				=> $data['no_retur'],
			'id_supplier'			=> $data['id_supplier'],
			'status'				=> $data['status'],
			'tanggal_retur'			=> isset($data['tanggal_retur']) ? date('Y-m-d', strtotime($data['tanggal_retur'])) : '',
			'note'					=> $data['note'],
			'subtotal'				=> number_unformat($data['subtotal']),
			'tax_rate'				=> number_unformat($data['tax_rate']),
			'tax'					=> number_unformat($data['tax']),
			'total'					=> number_unformat($data['total'])
		);

		$query = $this->Retur_model->updateData($data_header, $data_detail, $idretur);
		
		if($query)
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
		else
			$this->session->set_flashdata('error', 'Data gagal disimpan');
		
		redirect('Retur');

	}
}