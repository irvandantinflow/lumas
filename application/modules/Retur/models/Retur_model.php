<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Retur_model extends CI_Model 
{    
    var $table = 'retur_header';
    var $primary = 'id_retur';

    public function __construct(){
        parent::__construct(); 
    }

    public function getList(){
        $query = $this->db->query("SELECT a.*,
                                    CASE WHEN a.status = 'D' THEN 'Draft' WHEN a.status = 'O' THEN 'Open' ELSE '' END AS 'statusretur',
                                    b.nama_supplier
                                    FROM retur_header a
                                    LEFT JOIN supplier b ON a.id_supplier = b.id_supplier
                                    order by a.id_retur desc");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    public function getPOH($id){
        $query = $this->db->query("SELECT *, 'Draft' AS 'statusretur' FROM purchase_header WHERE id_purchase = '".$id."' AND status = 'O'");
        if ($query->num_rows() > 0){ 
            return $query;
        }else{
            $query->free_result();
            return $query;  
        }
    }

     public function getPOD($id){
        $query = $this->db->query("SELECT *
                                    FROM purchase_detail
                                    WHERE id_purchase = '".$id."'
                                    order by line_num ");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    function addData($data_header = '', $data_detail = ''){
    
        $this->db->trans_start();
        
        $id_purchase = $data_header['id_purchase'];

        // Insert Into salesorder_header Start
        $this->db->set('created_user', $this->session->userdata('user_id'));
        $this->db->set('created_date', 'now()', FALSE);
        $this->db->set($data_header);
        
        $query = $this->db->insert('retur_header');
        if(!$query) return false;
        $ID = $this->db->insert_id();
        // Insert Into salesorder_header End
        
        // Insert Into salesorder_detail Start
        for($i = 0; $i < count($data_detail); $i++){
            $this->db->set('id_retur', $ID, FALSE);
            $this->db->set('created_user', $this->session->userdata('user_id'));
            $this->db->set('created_date', 'now()', FALSE);
            $this->db->set($data_detail[$i]);
            
            $query = $this->db->insert('retur_detail');
            if(!$query) return false;
        }
        // Insert Into salesorder_detail End

        $this->db->set('status', 'C');
        $this->db->set('updated_user', $this->session->userdata('user_id'), FALSE);
        $this->db->set('updated_date', 'now()', FALSE);
        $this->db->where('id_purchase', $id_purchase);
        $query = $this->db->update('purchase_header');
        if(!$query) return false;

        $this->db->trans_complete();
        
        return true;
    }

    function updateData($data_header = '', $data_detail = '', $idretur = 0){

        $this->db->trans_start();

        // Update Into salesorder_header Start
        $this->db->set('updated_user', $this->session->userdata('user_id'), FALSE);
        $this->db->set('updated_date', 'now()', FALSE);
        $this->db->set($data_header); 
        $this->db->where('id_retur', $idretur); 
        $query = $this->db->update('retur_header');
        if(!$query) return false;
        // Update Into salesorder_header End

        // Update Into salesorder detail Start
        $itd    = '';
        foreach($data_detail as $row) {
            $itDetailID = $row['id_retur_detail'];
            unset($row['id_retur_detail']);
            
            if ($itDetailID > 0) { 
                
                $itd    .= ($itd == '') ? $itDetailID : ','.$itDetailID; 
                $this->db->set('id_retur', $idretur, FALSE);
                $this->db->set('updated_user', $this->session->userdata('user_id'));
                $this->db->set('updated_date', 'now()', FALSE);
                $this->db->set($row);
                $this->db->where('id_retur_detail', $itDetailID); 
                $query = $this->db->update('retur_detail'); 

            }else { 
                
                $this->db->set('id_retur', $idretur, FALSE);
                $this->db->set('created_user', $this->session->userdata('user_id'));
                $this->db->set('created_date', 'now()', FALSE);
                $this->db->set($row);
                $query = $this->db->insert('retur_detail'); 
                $ID = $this->db->insert_id();
                $itd    .= ($itd == '') ? $ID : ','.$ID; 

            } 
        }
        // Update Into salesorder_detail End

        if ($itd != '') {
            $this->db->query("DELETE FROM retur_detail where id_retur = ? and id_retur_detail not in (".$itd.")", $idretur);
        }

        if ($data_header['status'] == 'O')
        {
            // die(var_dump($data_detail));
            foreach($data_detail as $row) {

                // stok
                $check_stock = $this->db->query("SELECT * FROM stock WHERE kode_produk = '".$row['kode_produk']."' ");
                if($check_stock->num_rows() > 0){

                    $total = $check_stock->row_object()->quantity - ($row['quantity'] * $row['order_quantity']);

                    $this->db->set('quantity', $total);
                    $this->db->set('updated_user', $this->session->userdata('user_id'));
                    $this->db->set('updated_date', 'now()', FALSE);
                    $this->db->where('kode_produk', $row['kode_produk']);
                    $simpanstock = $this->db->update('stock');
                    if(!$simpanstock) return false;

                }

                // stok opname
                $this->db->set('no_retur', $data_header['no_retur']);
                $this->db->set('id_supplier', $data_header['id_supplier']);
                $this->db->set('tanggal_transaksi', $data_header['tanggal_retur']);
                $this->db->set('kode_produk', $row['kode_produk']);
                $this->db->set('quantity_keluar', ($row['quantity'] * $row['order_quantity']));
                $this->db->set('created_user', $this->session->userdata('user_id'));
                $this->db->set('created_date', 'now()', FALSE);
                $simpanstocknew = $this->db->insert('stock_opname');
                if(!$simpanstocknew) return false;

            }
        }
        //insert stok end

        $this->db->trans_complete(); 

        return true;
    }

    function checkID($id){
        $query = $this->db->get_where($this->table, array($this->primary=>$id));
        if ($query->num_rows() > 0) 
            return true;
        else
            return false;
    }

    function checkPOByID($id){
        $query = $this->db->get_where("purchase_header", array("id_purchase"=>$id));
        if ($query->num_rows() > 0) 
            return true;
        else
            return false;        
    }

    function checkPOStatusByID($id){
        $query = $this->db->query("SELECT * FROM `purchase_header` WHERE id_purchase = '".$id."' AND status = 'C'");
        if($query->num_rows() > 0)
            return FALSE;
        else
            return true;
    }

    public function edit($id){
        $query = $this->db->query("SELECT do.*, CASE WHEN do.status = 'D' THEN 'Draft' WHEN do.status = 'O' THEN 'Open' 
                    ELSE '' END AS 'statusretur', so.no_purchase 
                    FROM retur_header do INNER JOIN purchase_header so ON do.id_purchase = so.id_purchase
                    WHERE do.id_retur = '".$id."'");
        return $query;
    }

    public function editdetail($id){
        $query = $this->db->query("SELECT *
                                    FROM retur_detail
                                    WHERE id_retur = '".$id."'
                                    order by line_num ");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    function getSupplierbyPOID($id){
        $query = $this->db->query("SELECT c.nama_supplier, po.id_supplier FROM purchase_header po 
                                    INNER JOIN supplier c ON po.id_supplier=c.id_supplier 
                                    WHERE po.id_purchase = '".$id."'");
        if($query->num_rows()>0){
            return $query->first_row();
        } else {
            $query->free_result();
            return $query;
        }
    }

    function getSupplierbyRHID($id){
        $query = $this->db->query("SELECT c.nama_supplier, rh.id_supplier FROM retur_header rh 
                                    INNER JOIN supplier c ON rh.id_supplier=c.id_supplier 
                                    WHERE rh.id_retur = '".$id."'");
        if($query->num_rows()>0){
            return $query->first_row();
        } else {
            $query->free_result();
            return $query;
        }
    }
}   
