<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left"></div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group"></div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">

      <div class="col-md-6 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?php echo $title; ?> </h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <form class="form-horizontal form-label-left" method="POST" action="<?php echo $page_action;?>" >

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Username</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="text" class="form-control" placeholder="Username" name="username" value="<?php echo isset($get_data) ? $get_data->row()->username : ''; ?>">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Fullname</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="text" class="form-control" placeholder="Fullname" name="fullname" value="<?php echo isset($get_data) ? $get_data->row()->fullname : ''; ?>">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Password</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="password" class="form-control" placeholder="Password" id="new_password" name="new_password" >
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Re-type Password</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="password" class="form-control" placeholder="Re-type Password" id="retype_password" name="retype_password" ><span id="info_pass_match"></span>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Usergroup</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <select class="form-control select2_single" name="id_usergroup" required="">
                    <option value="">-- Select Usergroup --</option>
                    <?php foreach ($usergroup->result() as $baris) {
                      if (isset($get_data) && $baris->id_usergroup == $get_data->row()->id_usergroup) { ?>
                      <option value="<?php echo $baris->id_usergroup; ?>" selected><?php echo strtoupper($baris->usergroup); ?></option>
                       <?php } else { ?>
                      <option value="<?php echo $baris->id_usergroup; ?>"><?php echo strtoupper($baris->usergroup); ?></option>
                    <?php } }?>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Aktif</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <p>
                    YA <input type="radio" class="flat" name="aktif" value="Y" checked="" required <?php echo (isset($get_data) && $get_data->row()->aktif == 'Y') ? 'checked' : ''; ?>/> 
                    TIDAK <input type="radio" class="flat" name="aktif" value="N" <?php echo (isset($get_data) && $get_data->row()->aktif == 'N') ? 'checked' : ''; ?>/>
                  </p>
                </div>
              </div>

              <input type="hidden" name="user_id" value="<?php echo isset($get_data) ? $get_data->row()->user_id : ''; ?>"/>

              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                  <a href="<?php echo base_url('User'); ?>" class="btn btn-primary" name="cancel">Cancel</a>
                  <button type="submit" class="btn btn-success" name="submit">Submit</button>
                </div>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
        
    $('#retype_password').blur(function(){
      var pass1 = $('#new_password').val();
      var pass2 = $('#retype_password').val();
      if(pass1 === pass2){
        $('#info_pass_match').css("color","blue");
        $('#info_pass_match').html("");
        $('#info_pass_match').append('Match!');
      } else {
        $('#info_pass_match').css("color","red");
        $('#info_pass_match').html("");
        $('#info_pass_match').append('Not Match!');
      }
    });
        
  });
</script>