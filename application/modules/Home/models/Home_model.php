<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_model extends CI_Model 
{    
    public function __construct(){
        parent::__construct(); 
    }

    public function getSafetyStock(){
        $query = $this->db->query("SELECT T0.*, 
        							T1.nama_produk
        							FROM stock T0
        							LEFT JOIN produk T1 ON T0.kode_produk = T1.kode_produk
        							WHERE T0.quantity < 1000
        							order by T0.kode_produk asc");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    public function getSupplier(){
        $query = $this->db->query(" SELECT COUNT(*) AS total from supplier ");
        if ($query->num_rows() > 0) {
          return $query->first_row()->total;  
        } else {
          return $query;
        }
    }

    public function getPurchase(){
        $query = $this->db->query(" SELECT COUNT(*) AS total from purchase_header ");
        if ($query->num_rows() > 0) {
          return $query->first_row()->total;  
        } else {
          return $query;
        }
    }

    public function getCustomer(){
        $query = $this->db->query(" SELECT COUNT(*) AS total from customer ");
        if ($query->num_rows() > 0) {
          return $query->first_row()->total;  
        } else {
          return $query;
        }
    }

    public function getSales(){
        $query = $this->db->query(" SELECT COUNT(*) AS total from salesorder_header ");
        if ($query->num_rows() > 0) {
          return $query->first_row()->total;  
        } else {
          return $query;
        }
    }
}