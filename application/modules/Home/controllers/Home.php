<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Home_model');
		if(!is_logged_in()){redirect('Inadminpage');}	
	}

	public function index(){  
		
	   	$data = array(
	   		'active'			=> 'Rating',
	   		'content'			=> 'Home/home',
	   		'list_safetystock'		=> $this->Home_model->getSafetyStock(),
	   		'supplier'				=> $this->Home_model->getSupplier(),
	   		'purchase'				=> $this->Home_model->getPurchase(),
	   		'customer'				=> $this->Home_model->getCustomer(),
	   		'sales'					=> $this->Home_model->getSales()
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}
}