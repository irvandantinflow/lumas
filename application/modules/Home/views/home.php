        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <?php echo $this->load->view('alert');?>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search"></div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row top_tiles">
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-user"></i></div>
                  <div class="count"><?php echo $supplier; ?></div>
                  <h3>Data Supplier</h3>
                  <p>&nbsp;</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-area-chart"></i></div>
                  <div class="count"><?php echo $purchase; ?></div>
                  <h3>Data Purchase Order</h3>
                  <p>&nbsp;</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-users"></i></div>
                  <div class="count"><?php echo $customer; ?></div>
                  <h3>Data Customer</h3>
                  <p>&nbsp;</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-line-chart"></i></div>
                  <div class="count"><?php echo $sales; ?></div>
                  <h3>Data Sales Order</h3>
                  <p>&nbsp;</p>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Data stok yang harus dipesan </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Kode Produk</th>
                          <th>Nama Produk</th>
                          <th>Quantity</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
                        $no = 1;
                        if ($list_safetystock != '') :
                        foreach($list_safetystock->result() as $row){ 
                        ?>
                        <tr>
                          <td><?php echo $no++; ?></td>
                          <td><?php echo $row->kode_produk; ?></td>
                          <td><?php echo $row->nama_produk; ?></td>
                          <td><?php echo $row->quantity; ?></td>
                        </tr>
                        <?php } endif;?>
                      </tbody>
                    </table>

                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /page content -->