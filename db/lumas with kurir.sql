-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 23 Des 2018 pada 04.11
-- Versi Server: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lumas`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `customer`
--

CREATE TABLE `customer` (
  `id_customer` int(3) NOT NULL,
  `nama_customer` varchar(100) NOT NULL,
  `telp_customer` varchar(50) NOT NULL,
  `alamat_customer` text NOT NULL,
  `aktif` char(1) NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `customer`
--

INSERT INTO `customer` (`id_customer`, `nama_customer`, `telp_customer`, `alamat_customer`, `aktif`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(1, 'JAYA MANDIRI', '02112344321', 'JALAN LAMA BGT', 'Y', 1, '2018-11-19 19:49:16', 1, '2018-11-19 19:50:13');

-- --------------------------------------------------------

--
-- Struktur dari tabel `deliveryorder_detail`
--

CREATE TABLE `deliveryorder_detail` (
  `id_deliveryorder_detail` int(3) NOT NULL,
  `id_deliveryorder` int(3) NOT NULL,
  `kode_produk` varchar(50) NOT NULL,
  `nama_produk` varchar(100) NOT NULL,
  `quantity` int(3) NOT NULL,
  `order_quantity` int(3) NOT NULL,
  `harga` double NOT NULL,
  `line_num` int(3) NOT NULL,
  `line_total` double NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `deliveryorder_detail`
--

INSERT INTO `deliveryorder_detail` (`id_deliveryorder_detail`, `id_deliveryorder`, `kode_produk`, `nama_produk`, `quantity`, `order_quantity`, `harga`, `line_num`, `line_total`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(4, 3, 'B002', 'BOX CHAINWAX 128GR + PARTISI', 1300, 2, 1500, 0, 3900000, 1, '2018-12-23 09:41:39', 0, '0000-00-00 00:00:00'),
(5, 3, 'LJ1012', 'Eco Cool Radiator Coolant 1 Liter', 1200, 3, 10636, 1, 38289600, 1, '2018-12-23 09:41:39', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `deliveryorder_header`
--

CREATE TABLE `deliveryorder_header` (
  `id_deliveryorder` int(3) NOT NULL,
  `no_deliveryorder` varchar(20) NOT NULL,
  `id_customer` int(3) NOT NULL,
  `id_salesorder` int(3) NOT NULL,
  `id_kurir` int(3) NOT NULL,
  `status` char(1) NOT NULL,
  `tanggal_deliveryorder` date NOT NULL,
  `notedo` text NOT NULL,
  `subtotal` double NOT NULL,
  `tax_rate` int(2) NOT NULL,
  `tax` double NOT NULL,
  `total` int(11) NOT NULL,
  `termofpayment` varchar(10) NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `deliveryorder_header`
--

INSERT INTO `deliveryorder_header` (`id_deliveryorder`, `no_deliveryorder`, `id_customer`, `id_salesorder`, `id_kurir`, `status`, `tanggal_deliveryorder`, `notedo`, `subtotal`, `tax_rate`, `tax`, `total`, `termofpayment`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(3, 'wew11', 1, 1, 2, 'O', '2018-12-23', 'woke', 42189600, 10, 4218960, 46408560, 'Cash', 1, '2018-12-23 09:41:39', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `harga`
--

CREATE TABLE `harga` (
  `id_harga` int(3) NOT NULL,
  `kode_produk` varchar(50) NOT NULL,
  `harga` double NOT NULL,
  `aktif` char(1) NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `harga`
--

INSERT INTO `harga` (`id_harga`, `kode_produk`, `harga`, `aktif`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(4, 'B002', 1500, 'Y', 1, '2018-11-21 20:22:38', 1, '2018-12-14 19:37:23'),
(5, 'LJ1012', 10636, 'Y', 1, '2018-11-21 20:23:01', 0, '0000-00-00 00:00:00'),
(6, 'LJ1010', 10000, 'Y', 1, '2018-11-21 20:23:17', 0, '0000-00-00 00:00:00'),
(7, 'LJ1009', 10000, 'Y', 1, '2018-11-21 20:23:32', 0, '0000-00-00 00:00:00'),
(8, 'LJ1005', 16364, 'Y', 1, '2018-11-21 20:24:10', 0, '0000-00-00 00:00:00'),
(9, 'LJ1016', 19545, 'Y', 1, '2018-11-21 20:24:29', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kurir`
--

CREATE TABLE `kurir` (
  `id_kurir` int(3) NOT NULL,
  `nama_kurir` varchar(100) NOT NULL,
  `telp_kurir` varchar(50) NOT NULL,
  `alamat_kurir` text NOT NULL,
  `aktif` char(1) NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kurir`
--

INSERT INTO `kurir` (`id_kurir`, `nama_kurir`, `telp_kurir`, `alamat_kurir`, `aktif`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(1, 'ANUGRAH EXPRESS', '123 123 123', 'JALAN DI DALAM GANG', 'Y', 1, '2018-11-19 19:57:16', 1, '2018-11-19 19:57:54'),
(2, 'JNE', '111', '111', 'Y', 1, '2018-12-14 19:48:56', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk`
--

CREATE TABLE `produk` (
  `kode_produk` varchar(50) NOT NULL,
  `nama_produk` varchar(100) NOT NULL,
  `aktif` char(1) NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `produk`
--

INSERT INTO `produk` (`kode_produk`, `nama_produk`, `aktif`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
('B002', 'BOX CHAINWAX 128GR + PARTISI', 'Y', 1, '2018-11-20 17:48:31', 1, '2018-11-21 20:22:21'),
('LJ1005', 'Mr.Bolt Carburator & Injection Cleaner 500ml', 'Y', 1, '2018-11-20 17:52:38', 0, '0000-00-00 00:00:00'),
('LJ1009', 'Helmot 3IN1 Helmet Care Biru btl 150ml', 'Y', 1, '2018-11-20 17:51:54', 0, '0000-00-00 00:00:00'),
('LJ1010', 'Helmot 3IN1 Helmet Care Hijau btl 150ml', 'Y', 1, '2018-11-20 17:51:13', 0, '0000-00-00 00:00:00'),
('LJ1012', 'Eco Cool Radiator Coolant 1 Liter', 'Y', 1, '2018-11-20 17:50:18', 0, '0000-00-00 00:00:00'),
('LJ1016', 'Carbinjex Foam Engine Conditioner 250ml', 'Y', 1, '2018-11-20 17:53:21', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `purchase_detail`
--

CREATE TABLE `purchase_detail` (
  `id_purchase_detail` int(3) NOT NULL,
  `id_purchase` int(3) NOT NULL,
  `kode_produk` varchar(50) NOT NULL,
  `nama_produk` varchar(100) NOT NULL,
  `quantity` int(3) NOT NULL,
  `order_quantity` int(3) NOT NULL,
  `harga` double NOT NULL,
  `line_num` int(3) NOT NULL,
  `line_total` double NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `purchase_detail`
--

INSERT INTO `purchase_detail` (`id_purchase_detail`, `id_purchase`, `kode_produk`, `nama_produk`, `quantity`, `order_quantity`, `harga`, `line_num`, `line_total`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(1, 1, 'B002', 'BOX CHAINWAX 128GR + PARTISI', 1500, 2, 1500, 0, 4500000, 1, '2018-12-19 14:37:17', 1, '2018-12-19 14:39:32'),
(2, 2, 'B002', 'BOX CHAINWAX 128GR + PARTISI', 1200, 2, 1500, 0, 3600000, 1, '2018-12-19 14:40:41', 1, '2018-12-19 14:40:55'),
(3, 2, 'LJ1012', 'Eco Cool Radiator Coolant 1 Liter', 1300, 3, 10636, 1, 41480400, 1, '2018-12-19 14:40:41', 1, '2018-12-19 14:40:55'),
(4, 3, 'LJ1005', 'Mr.Bolt Carburator & Injection Cleaner 500ml', 1200, 2, 16364, 0, 39273600, 1, '2018-12-23 00:33:11', 1, '2018-12-23 00:33:46'),
(5, 3, 'LJ1016', 'Carbinjex Foam Engine Conditioner 250ml', 1100, 3, 19545, 1, 64498500, 1, '2018-12-23 00:33:11', 1, '2018-12-23 00:33:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `purchase_header`
--

CREATE TABLE `purchase_header` (
  `id_purchase` int(3) NOT NULL,
  `no_purchase` varchar(20) NOT NULL,
  `id_supplier` int(3) NOT NULL,
  `status` char(1) NOT NULL,
  `tanggal_purchase` date NOT NULL,
  `note` text NOT NULL,
  `subtotal` double NOT NULL,
  `tax_rate` int(2) NOT NULL,
  `tax` double NOT NULL,
  `other` double NOT NULL,
  `total` int(11) NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `purchase_header`
--

INSERT INTO `purchase_header` (`id_purchase`, `no_purchase`, `id_supplier`, `status`, `tanggal_purchase`, `note`, `subtotal`, `tax_rate`, `tax`, `other`, `total`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(1, '1', 1, 'O', '2018-12-19', '', 4500000, 10, 450000, 0, 4950000, 1, '2018-12-19 14:37:17', 1, '2018-12-19 14:39:32'),
(2, '2', 1, 'C', '2018-12-19', '', 45080400, 10, 4508040, 0, 49588440, 1, '2018-12-19 14:40:41', 1, '2018-12-22 22:28:56'),
(3, '1232', 2, 'C', '2018-12-22', 'catatan po', 103772100, 10, 10377210, 0, 114149310, 1, '2018-12-23 00:33:11', 1, '2018-12-23 00:35:02');

-- --------------------------------------------------------

--
-- Struktur dari tabel `retur_detail`
--

CREATE TABLE `retur_detail` (
  `id_retur_detail` int(3) NOT NULL,
  `id_retur` int(3) NOT NULL,
  `kode_produk` varchar(50) NOT NULL,
  `nama_produk` varchar(100) NOT NULL,
  `quantity` int(3) NOT NULL,
  `order_quantity` int(3) NOT NULL,
  `harga` double NOT NULL,
  `line_num` int(3) NOT NULL,
  `line_total` double NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `retur_detail`
--

INSERT INTO `retur_detail` (`id_retur_detail`, `id_retur`, `kode_produk`, `nama_produk`, `quantity`, `order_quantity`, `harga`, `line_num`, `line_total`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(4, 3, 'B002', 'BOX CHAINWAX 128GR + PARTISI', 500, 1, 1500, 0, 750000, 1, '2018-12-22 22:28:56', 1, '2018-12-22 22:58:33'),
(5, 3, 'LJ1012', 'Eco Cool Radiator Coolant 1 Liter', 500, 1, 10636, 1, 5318000, 1, '2018-12-22 22:28:56', 1, '2018-12-22 22:58:33'),
(6, 4, 'LJ1005', 'Mr.Bolt Carburator & Injection Cleaner 500ml', 1200, 2, 16364, 0, 39273600, 1, '2018-12-23 00:35:02', 1, '2018-12-23 00:35:27'),
(7, 4, 'LJ1016', 'Carbinjex Foam Engine Conditioner 250ml', 1100, 3, 19545, 1, 64498500, 1, '2018-12-23 00:35:02', 1, '2018-12-23 00:35:27');

-- --------------------------------------------------------

--
-- Struktur dari tabel `retur_header`
--

CREATE TABLE `retur_header` (
  `id_retur` int(3) NOT NULL,
  `no_retur` varchar(20) NOT NULL,
  `id_supplier` int(3) NOT NULL,
  `id_purchase` int(3) NOT NULL,
  `status` char(1) NOT NULL,
  `tanggal_retur` date NOT NULL,
  `note` text NOT NULL,
  `subtotal` double NOT NULL,
  `tax_rate` int(2) NOT NULL,
  `tax` double NOT NULL,
  `other` double NOT NULL,
  `total` int(11) NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `retur_header`
--

INSERT INTO `retur_header` (`id_retur`, `no_retur`, `id_supplier`, `id_purchase`, `status`, `tanggal_retur`, `note`, `subtotal`, `tax_rate`, `tax`, `other`, `total`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(3, '123', 1, 2, 'O', '2018-12-22', 'catatan', 6068000, 10, 606800, 0, 6674800, 1, '2018-12-22 22:28:56', 1, '2018-12-22 22:58:33'),
(4, '1243', 2, 3, 'O', '2018-12-22', 'catatan po', 103772100, 10, 10377210, 0, 114149310, 1, '2018-12-23 00:35:02', 1, '2018-12-23 00:35:27');

-- --------------------------------------------------------

--
-- Struktur dari tabel `salesorder_detail`
--

CREATE TABLE `salesorder_detail` (
  `id_salesorder_detail` int(3) NOT NULL,
  `id_salesorder` int(3) NOT NULL,
  `kode_produk` varchar(50) NOT NULL,
  `nama_produk` varchar(100) NOT NULL,
  `quantity` int(3) NOT NULL,
  `order_quantity` int(3) NOT NULL,
  `harga` double NOT NULL,
  `line_num` int(3) NOT NULL,
  `line_total` double NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `salesorder_detail`
--

INSERT INTO `salesorder_detail` (`id_salesorder_detail`, `id_salesorder`, `kode_produk`, `nama_produk`, `quantity`, `order_quantity`, `harga`, `line_num`, `line_total`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(1, 1, 'B002', 'BOX CHAINWAX 128GR + PARTISI', 1300, 2, 1500, 0, 3900000, 1, '2018-12-22 16:38:17', 1, '2018-12-22 16:38:30'),
(2, 1, 'LJ1012', 'Eco Cool Radiator Coolant 1 Liter', 1200, 3, 10636, 1, 38289600, 1, '2018-12-22 16:38:17', 1, '2018-12-22 16:38:30'),
(3, 2, 'LJ1012', 'Eco Cool Radiator Coolant 1 Liter', 10, 10, 10636, 0, 1063600, 1, '2018-12-23 09:38:50', 1, '2018-12-23 09:39:02'),
(4, 3, 'LJ1009', 'Helmot 3IN1 Helmet Care Biru btl 150ml', 10, 10, 10000, 0, 1000000, 1, '2018-12-23 09:47:37', 1, '2018-12-23 09:48:52'),
(5, 4, 'LJ1009', 'Helmot 3IN1 Helmet Care Biru btl 150ml', 10, 5, 10000, 0, 500000, 1, '2018-12-23 09:52:53', 1, '2018-12-23 09:53:03'),
(6, 5, 'LJ1009', 'Helmot 3IN1 Helmet Care Biru btl 150ml', 10, 100, 10000, 0, 10000000, 1, '2018-12-23 10:10:42', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `salesorder_header`
--

CREATE TABLE `salesorder_header` (
  `id_salesorder` int(3) NOT NULL,
  `no_salesorder` varchar(20) NOT NULL,
  `id_customer` int(3) NOT NULL,
  `status` char(1) NOT NULL,
  `tanggal_salesorder` date NOT NULL,
  `note` text NOT NULL,
  `subtotal` double NOT NULL,
  `tax_rate` int(2) NOT NULL,
  `tax` double NOT NULL,
  `total` int(11) NOT NULL,
  `termofpayment` varchar(10) NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `salesorder_header`
--

INSERT INTO `salesorder_header` (`id_salesorder`, `no_salesorder`, `id_customer`, `status`, `tanggal_salesorder`, `note`, `subtotal`, `tax_rate`, `tax`, `total`, `termofpayment`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(1, '1', 1, 'C', '2018-12-22', '', 42189600, 10, 4218960, 46408560, 'Cash', 1, '2018-12-22 16:38:17', 1, '2018-12-23 09:41:39'),
(2, 'asaa11', 1, 'O', '2018-12-23', 'oke oke jalah', 1063600, 10, 106360, 1169960, 'Transfer', 1, '2018-12-23 09:38:50', 1, '2018-12-23 09:40:07'),
(3, 'wokeeee', 1, 'O', '2018-12-23', 'woke', 1000000, 10, 100000, 1100000, 'Cash', 1, '2018-12-23 09:47:36', 1, '2018-12-23 09:48:51'),
(4, '12qwqw', 1, 'O', '2018-12-23', '', 500000, 10, 50000, 550000, 'Cash', 1, '2018-12-23 09:52:53', 1, '2018-12-23 09:53:03'),
(5, 'asasa', 1, 'D', '2018-12-23', '', 10000000, 10, 1000000, 11000000, 'Transfer', 1, '2018-12-23 10:10:41', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock`
--

CREATE TABLE `stock` (
  `kode_produk` varchar(50) NOT NULL,
  `quantity` int(3) NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `stock`
--

INSERT INTO `stock` (`kode_produk`, `quantity`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
('B002', 4900, 1, '2018-12-19 14:39:32', 1, '2018-12-22 22:58:33'),
('LJ1005', 0, 1, '2018-12-23 00:33:46', 1, '2018-12-23 00:35:27'),
('LJ1009', 50, 1, '2018-12-23 09:48:52', 1, '2018-12-23 09:53:03'),
('LJ1012', 3400, 1, '2018-12-19 14:40:55', 1, '2018-12-22 22:58:33'),
('LJ1016', 0, 1, '2018-12-23 00:33:46', 1, '2018-12-23 00:35:27');

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock_opname`
--

CREATE TABLE `stock_opname` (
  `id_stock` int(3) NOT NULL,
  `tanggal_transaksi` date NOT NULL,
  `no_purchase` varchar(20) NOT NULL,
  `no_salesorder` varchar(20) NOT NULL,
  `no_retur` varchar(20) NOT NULL,
  `id_supplier` int(3) NOT NULL,
  `id_customer` int(3) NOT NULL,
  `kode_produk` varchar(50) NOT NULL,
  `quantity_masuk` int(3) NOT NULL,
  `quantity_keluar` int(3) NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `stock_opname`
--

INSERT INTO `stock_opname` (`id_stock`, `tanggal_transaksi`, `no_purchase`, `no_salesorder`, `no_retur`, `id_supplier`, `id_customer`, `kode_produk`, `quantity_masuk`, `quantity_keluar`, `created_user`, `created_date`) VALUES
(1, '2018-12-19', '1', '', '', 1, 0, 'B002', 3000, 0, 1, '2018-12-19 14:39:32'),
(2, '2018-12-19', '2', '', '', 1, 0, 'B002', 2400, 0, 1, '2018-12-19 14:40:55'),
(3, '2018-12-19', '2', '', '', 1, 0, 'LJ1012', 3900, 0, 1, '2018-12-19 14:40:55'),
(4, '2018-12-22', '', '', '123', 1, 0, 'B002', 0, 500, 1, '2018-12-22 22:58:33'),
(5, '2018-12-22', '', '', '123', 1, 0, 'LJ1012', 0, 500, 1, '2018-12-22 22:58:33'),
(6, '2018-12-22', '1232', '', '', 2, 0, 'LJ1005', 2400, 0, 1, '2018-12-23 00:33:46'),
(7, '2018-12-22', '1232', '', '', 2, 0, 'LJ1016', 3300, 0, 1, '2018-12-23 00:33:46'),
(8, '2018-12-22', '', '', '1243', 2, 0, 'LJ1005', 0, 2400, 1, '2018-12-23 00:35:27'),
(9, '2018-12-22', '', '', '1243', 2, 0, 'LJ1016', 0, 3300, 1, '2018-12-23 00:35:27'),
(10, '2018-12-23', '', 'wokeeee', '', 0, 1, 'LJ1009', 0, 100, 1, '2018-12-23 09:48:52'),
(11, '2018-12-23', '', '12qwqw', '', 0, 1, 'LJ1009', 0, 50, 1, '2018-12-23 09:53:03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `supplier`
--

CREATE TABLE `supplier` (
  `id_supplier` int(3) NOT NULL,
  `nama_supplier` varchar(100) NOT NULL,
  `telp_supplier` varchar(50) NOT NULL,
  `alamat_supplier` text NOT NULL,
  `aktif` char(1) NOT NULL DEFAULT 'Y',
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `supplier`
--

INSERT INTO `supplier` (`id_supplier`, `nama_supplier`, `telp_supplier`, `alamat_supplier`, `aktif`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(1, 'INDAH BERSAMA', '02112344321', 'JL RAYA RAME BGT', 'Y', 1, '2018-11-19 19:25:46', 1, '2018-11-19 19:36:45'),
(2, 'MAJU SEJAHTERA', '1111222', '', 'Y', 1, '2018-12-23 00:24:37', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `user_id` int(3) NOT NULL,
  `username` varchar(30) NOT NULL,
  `fullname` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `id_usergroup` int(3) NOT NULL,
  `aktif` char(1) DEFAULT 'Y',
  `created_user` int(3) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_user` int(3) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`user_id`, `username`, `fullname`, `password`, `id_usergroup`, `aktif`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(1, 'admin', 'Administrator', '21232f297a57a5a743894a0e4a801fc3', 1, 'Y', 1, '2018-06-14 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `usergroup`
--

CREATE TABLE `usergroup` (
  `id_usergroup` int(3) NOT NULL,
  `usergroup` varchar(30) NOT NULL,
  `aktif` char(1) NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `usergroup`
--

INSERT INTO `usergroup` (`id_usergroup`, `usergroup`, `aktif`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(1, 'Administrator', 'Y', 1, '2018-06-14 00:00:00', 0, '0000-00-00 00:00:00'),
(2, 'Manager', 'Y', 1, '2018-11-20 17:35:17', 0, '0000-00-00 00:00:00'),
(3, 'Penjualan', 'Y', 1, '2018-11-20 17:35:47', 0, '0000-00-00 00:00:00'),
(4, 'Gudang', 'Y', 1, '2018-11-20 17:35:58', 0, '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indexes for table `deliveryorder_detail`
--
ALTER TABLE `deliveryorder_detail`
  ADD PRIMARY KEY (`id_deliveryorder_detail`);

--
-- Indexes for table `deliveryorder_header`
--
ALTER TABLE `deliveryorder_header`
  ADD PRIMARY KEY (`id_deliveryorder`);

--
-- Indexes for table `harga`
--
ALTER TABLE `harga`
  ADD PRIMARY KEY (`id_harga`);

--
-- Indexes for table `kurir`
--
ALTER TABLE `kurir`
  ADD PRIMARY KEY (`id_kurir`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`kode_produk`);

--
-- Indexes for table `purchase_detail`
--
ALTER TABLE `purchase_detail`
  ADD PRIMARY KEY (`id_purchase_detail`);

--
-- Indexes for table `purchase_header`
--
ALTER TABLE `purchase_header`
  ADD PRIMARY KEY (`id_purchase`);

--
-- Indexes for table `retur_detail`
--
ALTER TABLE `retur_detail`
  ADD PRIMARY KEY (`id_retur_detail`);

--
-- Indexes for table `retur_header`
--
ALTER TABLE `retur_header`
  ADD PRIMARY KEY (`id_retur`);

--
-- Indexes for table `salesorder_detail`
--
ALTER TABLE `salesorder_detail`
  ADD PRIMARY KEY (`id_salesorder_detail`);

--
-- Indexes for table `salesorder_header`
--
ALTER TABLE `salesorder_header`
  ADD PRIMARY KEY (`id_salesorder`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`kode_produk`);

--
-- Indexes for table `stock_opname`
--
ALTER TABLE `stock_opname`
  ADD PRIMARY KEY (`id_stock`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id_supplier`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `usergroup`
--
ALTER TABLE `usergroup`
  ADD PRIMARY KEY (`id_usergroup`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id_customer` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `deliveryorder_detail`
--
ALTER TABLE `deliveryorder_detail`
  MODIFY `id_deliveryorder_detail` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `deliveryorder_header`
--
ALTER TABLE `deliveryorder_header`
  MODIFY `id_deliveryorder` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `harga`
--
ALTER TABLE `harga`
  MODIFY `id_harga` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `kurir`
--
ALTER TABLE `kurir`
  MODIFY `id_kurir` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `purchase_detail`
--
ALTER TABLE `purchase_detail`
  MODIFY `id_purchase_detail` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `purchase_header`
--
ALTER TABLE `purchase_header`
  MODIFY `id_purchase` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `retur_detail`
--
ALTER TABLE `retur_detail`
  MODIFY `id_retur_detail` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `retur_header`
--
ALTER TABLE `retur_header`
  MODIFY `id_retur` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `salesorder_detail`
--
ALTER TABLE `salesorder_detail`
  MODIFY `id_salesorder_detail` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `salesorder_header`
--
ALTER TABLE `salesorder_header`
  MODIFY `id_salesorder` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `stock_opname`
--
ALTER TABLE `stock_opname`
  MODIFY `id_stock` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id_supplier` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `usergroup`
--
ALTER TABLE `usergroup`
  MODIFY `id_usergroup` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
