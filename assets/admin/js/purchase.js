;(function($){
    $('#morelines').click(function() { 
		$("#ItemLookup").click();  
	});

	$('#ItemLookup').click(function() { 
		$("#findItem").val(''); 
		$("#tblItem tbody tr").remove(); 
	});

	$('#myModalItemLookup').on('shown.bs.modal', function () {
        $('#findItem').focus();
    })

    $(document).on('blur', '#order_quantity, #quantity', function () {
    	var idx = -1; 
    	var elID;
    	var LineTotal;

    	idx = $(this).closest('tr').index();	 
		elID = $(this).attr('id');

		var qtypack  = (isNaN(parseInt($(this).closest('tr').find('#quantity').val()))) ? 0 : parseInt($(this).closest('tr').find('#quantity').val());
		var qty  = (isNaN(parseInt($(this).closest('tr').find('#order_quantity').val()))) ? 0 : parseInt($(this).closest('tr').find('#order_quantity').val());

		// set unitPrice
        var unitPrice = 0;
        if (!isNaN(parseFloat($(this).closest('tr').find('#harga').val()))) {
            unitPrice = $(this).closest('tr').find('#harga').val(); 
        }

        LineTotal = unitPrice * qty * qtypack;	

        $(this).closest('tr').find("td:eq(7)").html(accounting.formatNumber(LineTotal, 0, ".", ","));
        $(this).closest('tr').find('#line_total').val(parseFloat(LineTotal).toFixed(0)); 

        calcTotal();
    });

    function calcLineTotal() {

    	var docTotal  = 0;
    	var vat_sum   = 0;
    	var lineTotal = 0;

    	$('#datatables tbody tr.tblRowSO').each(function(){ 
    		lineTotal	= (isNaN(parseFloat($(this).closest('tr').find('#line_total').val()))) ? 0 : parseFloat($(this).closest('tr').find('#line_total').val());
    		vat_sum     += (lineTotal * 0.1);
    		docTotal    +=  lineTotal;
    	}); 

    	$('#subtotal').val(accounting.formatNumber(docTotal, 0, ".", ","));
    	$('#tax').val(accounting.formatNumber(vat_sum, 0, ".", ","));

    }

    function calcTotal() { 

    	calcLineTotal();

    	var docTotal  = 0;
    	var subTotal  = (isNaN(parseFloat(  accounting.unformat($('#subtotal').val(), ",") ))) ? 0 : parseFloat( accounting.unformat($('#subtotal').val(), ",")  );
    	var vat_sum   = (isNaN(parseFloat( accounting.unformat($('#tax').val(), ",") ))) ? 0 : parseFloat(  accounting.unformat($('#tax').val(), ","));

    	docTotal = parseFloat(subTotal) + parseFloat(vat_sum);

    	$('#total').val(accounting.formatNumber(docTotal, 0, ".", ","));  

    }


    // ================== delete lines ================== 
	$("#deletelines").click(function(){ 
		if ($('#datatables >tbody >tr').length > 1) {
			$('#datatables tbody tr').each(function(){   
				if ($(this).find('input.lineitem:checked').length >= 1 && $('#datatables >tbody >tr').length > 1) {
					$(this).remove(); // to remove, use $(this).remove();
				}
			});
			
			//re-assign row number
			$('#datatables tbody tr').each(function(){
				var iRow = $(this).index();
				$(this).find('td:eq(1)').html(iRow+1);
				$(this).find('#quantity').attr("name", 'order['+iRow+'][quantity]');
				$(this).find('#order_quantity').attr("name", 'order['+iRow+'][order_quantity]');
				$(this).find('#kode_produk').attr("name", 'order['+iRow+'][kode_produk]');
				$(this).find('#nama_produk').attr("name", 'order['+iRow+'][nama_produk]');
				$(this).find('#id_purchase').attr("name", 'order['+iRow+'][id_purchase]');
				$(this).find('#id_purchase_detail').attr("name", 'order['+iRow+'][id_purchase_detail]');
				$(this).find('#harga').attr("name", 'order['+iRow+'][harga]');
				$(this).find('#line_total').attr("name", 'order['+iRow+'][line_total]');
			});
		} 
        
        if ($('#datatables >tbody >tr').length == 1) {
		    var iRow = 0; 
            $('#datatables tbody tr').each(function(){   
				if ($(this).find('input.lineitem:checked').length >= 1 && $('#datatables >tbody >tr').length == 1) { 
					$(this).find('td:eq(1)').html(iRow+1);
    			    $(this).find('td:eq(2)').html('');
    			    $(this).find('td:eq(3)').html('');
    			    $(this).find('td:eq(6)').html('');
    			    $(this).find('td:eq(7)').html('');
    			    $(this).find('#quantity').attr("name", 'order['+iRow+'][quantity]');
    			    $(this).find('#order_quantity').attr("name", 'order['+iRow+'][order_quantity]');
    			    $(this).find('#kode_produk').attr("name", 'order['+iRow+'][kode_produk]');
    			    $(this).find('#nama_produk').attr("name", 'order['+iRow+'][nama_produk]');
    			    $(this).find('#id_purchase').attr("name", 'order['+iRow+'][id_purchase]');
    			    $(this).find('#id_purchase_detail').attr("name", 'order['+iRow+'][id_purchase_detail]');
    			    $(this).find('#harga').attr("name", 'order['+iRow+'][harga]');
    			    $(this).find('#line_total').attr("name", 'order['+iRow+'][line_total]');
					$(this).find('#quantity').val('');
					$(this).find('#order_quantity').val('');
                    $(this).find('#kode_produk').val('');
                    $(this).find('#nama_produk').val('');
                    $(this).find('#id_purchase').val('');
                    $(this).find('#id_purchase_detail').val('');
                    $(this).find('#harga').val('');
                    $(this).find('#line_total').val('');
				}
			}); 
		}

		calcTotal();
	});

    $('#initTable').click(function(){
		initializeTable($('#datatables'));
	});

	function initializeTable(jQtable) {
		jQtable.find("tr:gt(1)").remove();
		jQtable.find('tbody tr td:eq(1)').html(1);
		jQtable.find('tbody tr td:eq(2)').html('');
		jQtable.find('tbody tr td:eq(3)').html('');
		$(this).find('tbody tr td:eq(6)').html('');
    	$(this).find('tbody tr td:eq(7)').html('');
		jQtable.find('tbody tr #quantity').attr("name", 'order[0][quantity]');
		jQtable.find('tbody tr #order_quantity').attr("name", 'order[0][order_quantity]');
        jQtable.find('tbody tr #kode_produk').attr("name", 'order[0][kode_produk]');
        jQtable.find('tbody tr #nama_produk').attr("name", 'order[0][nama_produk]');
        jQtable.find('tbody tr #id_purchase').attr("name", 'order[0][id_purchase]');
        jQtable.find('tbody tr #id_purchase_detail').attr("name", 'order[0][id_purchase_detail]');
        jQtable.find('tbody tr #harga').attr("name", 'order[0][harga]');
        jQtable.find('tbody tr #line_total').attr("name", 'order[0][line_total]');
        jQtable.find('tbody tr #kode_produk').val('');
        jQtable.find('tbody tr #nama_produk').val('');
        jQtable.find('tbody tr #quantity').val('');
        jQtable.find('tbody tr #order_quantity').val('');
        jQtable.find('tbody tr #id_purchase').val('');
        jQtable.find('tbody tr #id_purchase_detail').val('');
        jQtable.find('tbody tr #harga').val('');
        jQtable.find('tbody tr #line_total').val('');
        calcTotal();
	}

	$('#datatables .group-checkable').change(function () {
		var set = jQuery(this).attr("data-set");
		var checked = jQuery(this).is(":checked");
		jQuery(set).each(function () {
			if (checked) {
				$(this).attr("checked", true);
				$(this).parents('tr').addClass("active");
			} else {
				$(this).attr("checked", false);
				$(this).parents('tr').removeClass("active");
			}   			
		});
		jQuery.uniform.update(set);

	});

}(jQuery));